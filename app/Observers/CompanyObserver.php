<?php

namespace App\Observers;

use App\Company;
use App\Notifications\Company\CompanyPublished;
use App\Notifications\Company\CompanyRejected;

class CompanyObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Company $company
     * @return void
     */
    public function created(Company $company)
    {
        //
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Company $company
     * @return void
     */
    public function updated(Company $company)
    {
        if ($company->user && $company->isDirty('published') && $company->published) {
            $company->user->notify(new CompanyPublished($company));
        }

        if ($company->user && $company->isDirty('rejected') && $company->rejected) {
            $company->user->notify(new CompanyRejected($company));
        }
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Company $company
     * @return void
     */
    public function deleted(Company $company)
    {
        //
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Company $company
     * @return void
     */
    public function restored(Company $company)
    {
        //
    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Company $company
     * @return void
     */
    public function forceDeleted(Company $company)
    {
        //
    }
}
