<?php

namespace App\Observers;

use App\Event;
use App\EventLabel;
use App\Notifications\Event\EventPublished;
use App\Notifications\Event\EventRejected;

class EventObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Event $event
     * @return void
     */
    public function created(Event $event)
    {
        if (!empty($label = EventLabel::where('name', 'Новое событие')->first())) {
            $event->labels()->syncWithoutDetaching([$label->id => [
                'start' => $event->start,
                'end' => $event->start->addDays(3),
            ]]);
        }
        if (!empty($label = EventLabel::where('name', 'Успейте воспользоваться')->first())) {
            $event->labels()->syncWithoutDetaching([$label->id => [
                'start' => $event->end->subDays(3),
                'end' => $event->end,
            ]]);
        }
    }

    /**
     * Handle the event "updated" event.
     *
     * @param  \App\Event $event
     * @return void
     */
    public function updated(Event $event)
    {
        if ($event->isDirty('start') && !empty($label = EventLabel::where('name', 'Новое событие')->first())) {
            $event->labels()->syncWithoutDetaching([$label->id => [
                'start' => $event->start,
                'end' => $event->start->addDays(3),
            ]]);
        }
        if ($event->isDirty('end') && !empty($label = EventLabel::where('name', 'Успейте воспользоваться')->first())) {
            $event->labels()->syncWithoutDetaching([$label->id => [
                'start' => $event->end->subDays(3),
                'end' => $event->end,
            ]]);
        }

        if ($event->user && $event->isDirty('published') && $event->published) {
            $event->user->notify(new EventPublished($event));
        }

        if ($event->user && $event->isDirty('rejected') && $event->rejected) {
            $event->user->notify(new EventRejected($event));
        }
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Event $event
     * @return void
     */
    public function deleted(Event $event)
    {
        //
    }

    /**
     * Handle the event "restored" event.
     *
     * @param  \App\Event $event
     * @return void
     */
    public function restored(Event $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param  \App\Event $event
     * @return void
     */
    public function forceDeleted(Event $event)
    {
        //
    }
}
