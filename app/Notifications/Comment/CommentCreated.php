<?php

namespace App\Notifications\Comment;

use App\Comment;
use App\Company;
use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class CommentCreated extends Notification
{
    use Queueable;

    public $comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if ($this->comment->commented_type == Event::class) {
            return [
                'text' => "На ваше событие поступил <a href='" . route('cabinet.comment.show', $this->comment) . "'>отзыв</a>"
            ];
        } else if ($this->comment->commented_type == Company::class) {
            return [
                'text' => "На вашу компанию поступил <a href='" . route('cabinet.comment.show', $this->comment) . "'>отзыв</a>"
            ];
        }

        return [];
    }
}
