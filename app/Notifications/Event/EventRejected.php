<?php

namespace App\Notifications\Event;

use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EventRejected extends Notification
{
    use Queueable;

    public $event;

    /**
     * Create a new notification instance.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'text' => "Ваше событие <a href='" . route('frontend.event.show', $this->event) . "'>" . $this->event->name . "</a> отклонено" . (empty($this->event->message) ? "" : " (" . $this->event->message . ")")
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Ваше событие ' . $this->event->name . ' отклонено')
            ->greeting('Приветствуем!')
            ->line('Ваше событие ' . $this->event->name . ' отклонено!' . (empty($this->event->message) ? '' : ' ' . $this->event->message))
            ->line('Благодарим за использование нашего портала!');
    }
}
