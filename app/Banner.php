<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['start', 'end', 'link'];

    protected $dates = ['start', 'end'];

    public function download()
    {
        return $this->belongsTo(Download::class);
    }

    public function place()
    {
        return $this->belongsTo(BannerPlace::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
