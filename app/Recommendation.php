<?php

namespace App;

use App\Events\Recommendation\RecommendationCreated;
use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $fillable = ['bool'];

    protected $dispatchesEvents = [
        'created' => RecommendationCreated::class
    ];

    public function recommendable()
    {
        return $this->morphTo();
    }
}
