<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Storage;

class EventCategory extends Model
{
    use Sluggable;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $fillable = ['name', 'slug', 'image_download_id', 'background_download_id'];

    protected $appends = ['image_url', 'background_url'];

    public function image()
    {
        return $this->belongsTo(Download::class, 'image_download_id');
    }

    public function background()
    {
        return $this->belongsTo(Download::class, 'background_download_id');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_has_event_categories');
    }

    public function getImageUrlAttribute()
    {
        if (empty($this->image)) {
            return 'http://via.placeholder.com/510x600';
        }
        return Storage::url($this->image->path);
    }

    public function getBackgroundUrlAttribute()
    {
        if (empty($this->background)) {
            return 'http://via.placeholder.com/1440x750';
        }
        return Storage::url($this->background->path);
    }
}
