<?php

namespace App\Providers;

use App\City;
use App\Company;
use App\Event;
use App\Observers\CompanyObserver;
use App\Observers\EventObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function ($view) {
            $view->with('userCity', city());
            if (empty($view->city)) {
                $view->with('city', city());
            }
        });

        Event::observe(EventObserver::class);
        Company::observe(CompanyObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
