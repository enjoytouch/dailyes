<?php

namespace App\Http\Controllers;

use App\CompanyComment;
use Illuminate\Http\Request;

class CompanyCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyComment  $companyComment
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyComment $companyComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyComment  $companyComment
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyComment $companyComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyComment  $companyComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyComment $companyComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyComment  $companyComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyComment $companyComment)
    {
        //
    }
}
