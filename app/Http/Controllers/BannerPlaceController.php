<?php

namespace App\Http\Controllers;

use App\BannerPlace;
use Illuminate\Http\Request;

class BannerPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BannerPlace  $bannerPlace
     * @return \Illuminate\Http\Response
     */
    public function show(BannerPlace $bannerPlace)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BannerPlace  $bannerPlace
     * @return \Illuminate\Http\Response
     */
    public function edit(BannerPlace $bannerPlace)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BannerPlace  $bannerPlace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BannerPlace $bannerPlace)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BannerPlace  $bannerPlace
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannerPlace $bannerPlace)
    {
        //
    }
}
