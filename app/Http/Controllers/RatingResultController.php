<?php

namespace App\Http\Controllers;

use App\RatingResult;
use Illuminate\Http\Request;

class RatingResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RatingResult  $ratingResult
     * @return \Illuminate\Http\Response
     */
    public function show(RatingResult $ratingResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RatingResult  $ratingResult
     * @return \Illuminate\Http\Response
     */
    public function edit(RatingResult $ratingResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RatingResult  $ratingResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RatingResult $ratingResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RatingResult  $ratingResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(RatingResult $ratingResult)
    {
        //
    }
}
