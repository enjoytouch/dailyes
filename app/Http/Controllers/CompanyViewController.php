<?php

namespace App\Http\Controllers;

use App\CompanyView;
use Illuminate\Http\Request;

class CompanyViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyView  $companyView
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyView $companyView)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyView  $companyView
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyView $companyView)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyView  $companyView
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyView $companyView)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyView  $companyView
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyView $companyView)
    {
        //
    }
}
