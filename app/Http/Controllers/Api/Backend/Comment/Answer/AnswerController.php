<?php

namespace App\Http\Controllers\Api\Backend\Comment\Answer;

use App\CommentAnswer as Answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function filter(Request $request)
    {
        $query = Answer::with([
            'user'
        ]);

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $items;
    }

    public function destroy(Answer $answer)
    {
        return Answer::destroy($answer->id);
    }

    public function toggle(Request $request, Answer $answer)
    {
        $answer->published = !$answer->published;
        $answer->save();

        return 'ok';
    }
}
