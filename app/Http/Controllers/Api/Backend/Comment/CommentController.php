<?php

namespace App\Http\Controllers\Api\Backend\Comment;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function filter(Request $request)
    {
        $query = Comment::with([
            'user'
        ]);

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $items;
    }

    public function destroy(Comment $comment)
    {
        return Comment::destroy($comment->id);
    }

    public function toggle(Request $request, Comment $comment)
    {
        $comment->published = !$comment->published;
        $comment->save();

        return 'ok';
    }
}
