<?php

namespace App\Http\Controllers\Api\Backend\Company;

use App\Address;
use App\City;
use App\Company;
use App\Download;
use App\GalleryItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter(Request $request)
    {
        $query = Company::query();

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:companies,name',
            'slug' => 'nullable|string|unique:companies,slug',
            'url' => 'nullable|url',
            'summary' => 'nullable|string',
            'about' => 'nullable|string',
            'published' => 'required|boolean',
            'rejected' => 'required|boolean',
            'active' => 'required|boolean',
            'message' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'gallery_items' => 'array',
            'categories.*.id' => 'required|exists:company_categories,id',
            'addresses.*.city.name' => 'required|string',
            'addresses.*.city.lat' => 'required|numeric',
            'addresses.*.city.long' => 'required|numeric',
            'addresses.*.address' => 'required|string',
            'addresses.*.phone' => 'nullable|string',
            'addresses.*.work' => 'nullable|string',
            'addresses.*.lat' => 'required|numeric',
            'addresses.*.long' => 'required|numeric',
            'addresses.*.site' => 'string'
        ]);

        $company = Company::make($validData);
        $company->image()->associate(Download::find($validData['image']['id']));
        $company->background()->associate(Download::find($validData['background']['id']));
        $company->save();

        $company->addresses()->saveMany(array_reduce(
            $request->addresses,
            function ($res, $a) {
                // Вытаскиваем по названию город либо создаем новый
                $city = City::firstOrCreate([
                    'name' => $a['city']['name']
                ], [
                    'lat' => $a['city']['lat'],
                    'long' => $a['city']['long']
                ]);
                $address = Address::make([
                    'address' => $a['address'],
                    'phone' => $a['phone'] ?? null,
                    'work' => $a['work'] ?? null,
                    'site' => $a['site'] ?? null,
                    'lat' => $a['lat'],
                    'long' => $a['long']
                ]);
                $address->city()->associate($city);

                array_push($res, $address);
                return $res;
            },
            []
        ));

        $company->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Галерея
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        foreach ($galleryItemIds as $id) {
            $company->gallery_items()->save(GalleryItem::find($id));
        }

        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Company $company
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Company $company)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:companies,name,' . $request->id,
            'slug' => 'nullable|string|unique:companies,slug,' . $request->id,
            'url' => 'nullable|url',
            'summary' => 'nullable|string',
            'about' => 'nullable|string',
            'published' => 'required|boolean',
            'rejected' => 'required|boolean',
            'active' => 'required|boolean',
            'message' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'gallery_items' => 'array',
            'categories.*.id' => 'required|exists:company_categories,id'
        ]);

        $company->fill($validData);
        $company->image()->associate(Download::find($validData['image']['id']));
        $company->background()->associate(Download::find($validData['background']['id']));
        $company->save();

        // Удаление лишних адресов, которые есть сейчас в базе, но в реквесте не пришли
        Address::destroy($company->addresses->pluck('id')->diff(Arr::pluck($request->addresses, 'id'))->values());

        $company->addresses()->saveMany(array_reduce(
            $request->addresses,
            function ($res, $a) {
                // Вытаскиваем по названию город либо создаем новый
                $city = City::firstOrCreate([
                    'name' => $a['city']['name']
                ], [
                    'lat' => $a['city']['lat'],
                    'long' => $a['city']['long']
                ]);
                if (!empty($a['id'])) {
                    $address = Address::find($a['id']);
                    $address->update($a);
                } else {
                    $address = Address::make([
                        'address' => $a['address'],
                        'phone' => $a['phone'] ?? null,
                        'work' => $a['work'] ?? null,
                        'site' => $a['site'] ?? null,
                        'lat' => $a['lat'],
                        'long' => $a['long']
                    ]);
                    array_push($res, $address);
                }
                $address->city()->associate($city);

                return $res;
            },
            []
        ));

        $company->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Обновление галереи
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        GalleryItem::destroy($company->gallery_items->pluck('id')->diff($galleryItemIds));
        foreach ($galleryItemIds as $id) {
            // TODO на данный момент если существующее изображение изменится, в базе оно не обновится
            $company->gallery_items()->save(GalleryItem::find($id));
        }

        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return int
     */
    public function destroy(Company $company)
    {
        return Company::destroy($company->id);
    }

    public function count()
    {
        return Company::count();
    }

    // функция производит поиск компаний по базе по переданной строке
    // если строка является числом, то происходит точная выборка id
    // если строка, то поиск идет только по названию компании
    public function search(Request $request, $string)
    {
        $query = Company::query();

        if ($string) {
            $query
                ->where('name', 'LIKE', '%' . $string . '%')
                ->when($id = filter_var($string, FILTER_VALIDATE_INT), function ($query) use ($id) {
                    $query->orWhere('id', $id);
                });
        }

        $items = $query->get();

        return $items;
    }
}
