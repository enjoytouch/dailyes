<?php

namespace App\Http\Controllers\Api\Backend\Event;

use App\Company;
use App\Download;
use App\Event;
use App\GalleryItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter(Request $request)
    {
        $query = Event::query();

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'company.id' => 'required|exists:companies,id',
            'name' => 'required|string|unique:events,name',
            'slug' => 'nullable|string|unique:events,slug',
            'summary' => 'nullable|string',
            'published' => 'required|boolean',
            'rejected' => 'required|boolean',
            'active' => 'required|boolean',
            'message' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'start' => 'required|date_format:Y-m-d H:i:s',
            'end' => 'required|date_format:Y-m-d H:i:s',
            'about' => 'nullable|string',
            'gallery_items' => 'array',
            'addresses' => 'array',
            'categories' => 'required|array',
            'categories.*.id' => 'required|exists:event_categories,id',
            'labels.*.id' => 'nullable|exists:event_labels,id',
            'favorite' => 'required|boolean'
        ]);

        $event = Event::make($validData);
        $event->company()->associate(Company::find($validData['company']['id']));
        $event->image()->associate(Download::find($validData['image']['id']));
        $event->background()->associate(Download::find($validData['background']['id']));
        $event->save();

        $event->addresses()->attach(Arr::pluck($validData['addresses'], 'id'));
        $event->categories()->attach(Arr::pluck($validData['categories'], 'id'));
        $event->labels()->attach(Arr::pluck($validData['labels'] ?? [], 'id'));

        // Галерея
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        foreach ($galleryItemIds as $id) {
            $event->gallery_items()->save(GalleryItem::find($id));
        }

        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories',
            'labels'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories',
            'labels'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'company.id' => 'required|exists:companies,id',
            'name' => 'required|string|unique:events,name,' . $request->id,
            'slug' => 'required|string|unique:events,slug,' . $request->id,
            'summary' => 'nullable|string',
            'published' => 'required|boolean',
            'rejected' => 'required|boolean',
            'active' => 'required|boolean',
            'message' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'start' => 'required|date_format:Y-m-d H:i:s',
            'end' => 'required|date_format:Y-m-d H:i:s',
            'about' => 'nullable|string',
            'gallery_items' => 'array',
            'addresses' => 'array',
            'categories' => 'required|array',
            'categories.*.id' => 'required|exists:event_categories,id',
            'labels.*.id' => 'nullable|exists:event_labels,id',
            'favorite' => 'required|boolean'
        ]);

        $event->fill($validData);
        $event->company()->associate(Company::find($validData['company']['id']));
        $event->image()->associate(Download::find($validData['image']['id']));
        $event->background()->associate(Download::find($validData['background']['id']));
        $event->addresses()->sync(Arr::pluck($validData['addresses'], 'id'));
        $event->categories()->sync(Arr::pluck($validData['categories'], 'id'));
        $event->labels()->sync(Arr::pluck($validData['labels'] ?? [], 'id'));
        $event->save();

        // Обновление галереи
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        GalleryItem::destroy($event->gallery_items->pluck('id')->diff($galleryItemIds));
        foreach ($galleryItemIds as $id) {
            // TODO на данный момент если существующее изображение изменится, в базе оно не обновится
            $event->gallery_items()->save(GalleryItem::find($id));
        }

        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories',
            'labels'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return int
     */
    public function destroy(Event $event)
    {
        return Event::destroy($event->id);
    }

    public function count()
    {
        return Event::count();
    }

    // функция производит поиск событий по базе по переданной строке
    // если строка является числом, то происходит точная выборка id
    // если строка, то поиск идет только по названию события
    public function search(Request $request, $string)
    {
        $query = Event::query();

        if ($string) {
            $query
                ->where('name', 'LIKE', '%' . $string . '%')
                ->when($id = filter_var($string, FILTER_VALIDATE_INT), function ($query) use ($id) {
                    $query->orWhere('id', $id);
                });
        }

        $items = $query->get();

        return $items;
    }
}
