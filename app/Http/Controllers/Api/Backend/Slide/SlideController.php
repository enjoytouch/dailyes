<?php

namespace App\Http\Controllers\Api\Backend\Slide;

use App\City;
use App\Download;
use App\Selection;
use App\Slide;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter(Request $request)
    {
        $query = Slide::query();

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_slide'));

        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:slides,name',
            'summary' => 'required|string',
            'button' => 'required|string',
            'image.id' => 'required|exists:downloads,id',
            'selection.id' => 'required|exists:selections,id',
            'city.id' => 'required|exists:cities,id'
        ]);

        $slide = Slide::make($validData);

        $image = Download::find($validData['image']['id']);
        $selection = Selection::find($validData['selection']['id']);

        $slide->image()->associate($image);
        $slide->selection()->associate($selection);
        $slide->city()->associate(City::find($validData['city']['id']));

        $slide->save();

        return $slide;
    }

    /**
     * Display the specified resource.
     *
     * @param Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        return $slide->load([
            'selection',
            'image',
            'city'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Slide $slide
     * @return Slide
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Slide $slide)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:slides,name,' . $request->id,
            'summary' => 'required|string',
            'button' => 'required|string',
            'image.id' => 'required|exists:downloads,id',
            'selection.id' => 'required|exists:selections,id',
            'city.id' => 'required|exists:cities,id'
        ]);

        $slide->fill($validData);

        $image = Download::find($validData['image']['id']);
        $selection = Selection::find($validData['selection']['id']);

        $slide->image()->associate($image);
        $slide->selection()->associate($selection);
        $slide->city()->associate(City::find($validData['city']['id']));

        $slide->save();

        return $slide;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Slide $slide
     * @return int
     */
    public function destroy(Slide $slide)
    {
        return Slide::destroy($slide->id);
    }
}
