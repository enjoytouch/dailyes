<?php

namespace App\Http\Controllers\Api\Backend\Report;

use App\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function filter(Request $request)
    {
        $query = Report::with([
            'user',
            'reportable'
        ]);

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $items;
    }
}
