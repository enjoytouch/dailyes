<?php

namespace App\Http\Controllers\Api\Backend\Tile;

use App\City;
use App\Selection;
use App\Tile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter(Request $request)
    {
        $query = Tile::query();

        if ($request->search) {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $items = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_tile'));

        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:tiles,name',
            'summary' => 'required|string',
            'selection.id' => 'required|exists:selections,id',
            'city.id' => 'required|exists:cities,id'
        ]);

        $tile = Tile::make($validData);

        $selection = Selection::find($validData['selection']['id']);

        $tile->selection()->associate($selection);
        $tile->city()->associate(City::find($validData['city']['id']));

        $tile->save();

        return $tile;
    }

    /**
     * Display the specified resource.
     *
     * @param Tile $tile
     * @return \Illuminate\Http\Response
     */
    public function show(Tile $tile)
    {
        return $tile->load([
            'selection',
            'city'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Tile $tile
     * @return Tile
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Tile $tile)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:tiles,name,' . $request->id,
            'summary' => 'required|string',
            'selection.id' => 'required|exists:selections,id',
            'city.id' => 'required|exists:cities,id'
        ]);

        $tile->fill($validData);

        $selection = Selection::find($validData['selection']['id']);

        $tile->selection()->associate($selection);
        $tile->city()->associate(City::find($validData['city']['id']));

        $tile->save();

        return $tile;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tile $tile
     * @return int
     */
    public function destroy(Tile $tile)
    {
        return Tile::destroy($tile->id);
    }
}
