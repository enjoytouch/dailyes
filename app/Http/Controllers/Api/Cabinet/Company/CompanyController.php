<?php

namespace App\Http\Controllers\Api\Cabinet\Company;

use App\Company;
use App\Download;
use App\GalleryItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CompanyController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'name' => 'required|string|unique:companies,name',
            'url' => 'nullable|url',
            'summary' => 'nullable|string',
            'about' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'gallery_items' => 'array',
            'categories.*.id' => 'required|exists:company_categories,id'
        ]);

        $company = Company::make($validData);
        $company->image()->associate(Download::find($validData['image']['id']));
        $company->background()->associate(Download::find($validData['background']['id']));
        $company->user()->associate(\Auth::user());
        $company->save();

        $company->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Галерея
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        foreach ($galleryItemIds as $id) {
            $company->gallery_items()->save(GalleryItem::find($id));
        }

        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        if ($company->user->id != \Auth::id()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Company $company
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Company $company)
    {
        if ($company->user->id != \Auth::id()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $validData = $this->validate($request, [
            'name' => 'required|string|unique:companies,name,' . $request->id,
            'url' => 'nullable|url',
            'summary' => 'nullable|string',
            'about' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'gallery_items' => 'array',
            'categories.*.id' => 'required|exists:company_categories,id'
        ]);

        $company->fill($validData);
        $company->image()->associate(Download::find($validData['image']['id']));
        $company->background()->associate(Download::find($validData['background']['id']));
        $company->user()->associate(\Auth::user());
        $company->save();

        $company->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Обновление галереи
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        GalleryItem::destroy($company->gallery_items->pluck('id')->diff($galleryItemIds));
        foreach ($galleryItemIds as $id) {
            // TODO на данный момент если существующее изображение изменится, в базе оно не обновится
            $company->gallery_items()->save(GalleryItem::find($id));
        }

        return $company->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'categories'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return int
     */
    public function destroy(Company $company)
    {
        if ($company->user->id != \Auth::id()) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return Company::destroy($company->id);
    }

    // функция производит поиск компаний по базе по переданной строке
    // если строка является числом, то происходит точная выборка id
    // если строка, то поиск идет только по названию компании
    public function search(Request $request, $string)
    {
        $query = Company::whereHas('user', function ($query) {
            $query->where('id', \Auth::id());
        });

        if ($string) {
            $query->where(function ($query) use ($string) {
                $query
                    ->where('name', 'LIKE', '%' . $string . '%')
                    ->when($id = filter_var($string, FILTER_VALIDATE_INT), function ($query) use ($id) {
                        $query->orWhere('id', $id);
                    });
            });
        }

        $items = $query->get();

        return $items;
    }
}
