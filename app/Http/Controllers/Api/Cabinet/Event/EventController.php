<?php

namespace App\Http\Controllers\Api\Cabinet\Event;

use App\Company;
use App\Download;
use App\Event;
use App\GalleryItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EventController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validData = $this->validate($request, [
            'company.id' => 'required|exists:companies,id',
            'name' => 'required|string|unique:events,name',
            'summary' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'start' => 'required|date_format:Y-m-d H:i:s',
            'end' => 'required|date_format:Y-m-d H:i:s',
            'about' => 'nullable|string',
            'gallery_items' => 'array',
            'addresses' => 'array',
            'categories.*.id' => 'required|exists:event_categories,id'
        ]);

        $event = Event::make($validData);
        $event->company()->associate(Company::find($validData['company']['id']));
        $event->image()->associate(Download::find($validData['image']['id']));
        $event->background()->associate(Download::find($validData['background']['id']));
        $event->save();

        $event->addresses()->sync(Arr::pluck($validData['addresses'], 'id'));
        $event->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Галерея
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        foreach ($galleryItemIds as $id) {
            $event->gallery_items()->save(GalleryItem::find($id));
        }

        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        if ($event->user->id != \Auth::id()) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Event $event)
    {
        if ($event->user->id != \Auth::id()) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $validData = $this->validate($request, [
            'company.id' => 'required|exists:companies,id',
            'name' => 'required|string|unique:events,name,' . $request->id,
            'summary' => 'nullable|string',
            'image.id' => 'required|exists:downloads,id',
            'background.id' => 'required|exists:downloads,id',
            'start' => 'required|date_format:Y-m-d H:i:s',
            'end' => 'required|date_format:Y-m-d H:i:s',
            'about' => 'nullable|string',
            'gallery_items' => 'array',
            'addresses' => 'array',
            'categories.*.id' => 'required|exists:event_categories,id',
            'active' => 'boolean'
        ]);

        $event->fill($validData);
        $event->company()->associate(Company::find($validData['company']['id']));
        $event->image()->associate(Download::find($validData['image']['id']));
        $event->background()->associate(Download::find($validData['background']['id']));
        $event->save();

        $event->addresses()->sync(Arr::pluck($validData['addresses'], 'id'));
        $event->categories()->sync(Arr::pluck($validData['categories'], 'id'));

        // Обновление галереи
        $galleryItemIds = Arr::pluck($validData['gallery_items'] ?? [], 'id');
        GalleryItem::destroy($event->gallery_items->pluck('id')->diff($galleryItemIds));
        foreach ($galleryItemIds as $id) {
            // TODO на данный момент если существующее изображение изменится, в базе оно не обновится
            $event->gallery_items()->save(GalleryItem::find($id));
        }

        return $event->load([
            'gallery_items',
            'gallery_items.attachable',
            'background',
            'image',
            'addresses',
            'addresses.city',
            'company',
            'company.addresses',
            'categories'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return int
     */
    public function destroy(Event $event)
    {
        if ($event->user->id != \Auth::id()) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return Event::destroy($event->id);
    }

    // функция производит поиск событий по базе по переданной строке
    // если строка является числом, то происходит точная выборка id
    // если строка, то поиск идет только по названию события
    public function search(Request $request, $string)
    {
        $query = Event::whereHas('user', function ($query) {
            $query->where('id', \Auth::id());
        });

        if ($string) {
            $query->where(function ($query) use ($string) {
                $query
                    ->where('name', 'LIKE', '%' . $string . '%')
                    ->when($id = filter_var($string, FILTER_VALIDATE_INT), function ($query) use ($id) {
                        $query->orWhere('id', $id);
                    });
            });
        }

        $items = $query->get();

        return $items;
    }
}
