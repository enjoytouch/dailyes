<?php

namespace App\Http\Controllers\Api\Frontend\City;

use App\Http\Controllers\Controller;
use App\City;

class CityController extends Controller
{
    // ближайший город
    public function near()
    {
        return City::near()->first();
    }

    public function all()
    {
        return City::all();
    }
}
