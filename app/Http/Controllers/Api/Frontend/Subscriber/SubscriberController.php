<?php

namespace App\Http\Controllers\Api\Frontend\Subscriber;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function subscribe(Request $request)
    {
        $validData = $this->validate($request, [
            'email' => 'required|email|unique:subscribers,email'
        ]);

        $subscriber = Subscriber::create($validData);

        return $subscriber;
    }
}
