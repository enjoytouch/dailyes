<?php

namespace App\Http\Controllers\Api\Frontend\Company;

use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function feature(Request $request)
    {
        $validData = $this->validate($request, [
            'slug' => 'required|exists:companies,slug'
        ]);

        $company = Company::where('slug', $validData['slug'])->firstOrFail();

        return $company->feature();
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function rating(Request $request, Company $company)
    {
        $validData = $this->validate($request, [
            'star' => 'required|numeric|between:1,5'
        ]);

        return $company->ratings()->save($rating = auth()->user()->ratings()->make([
            'star' => $validData['star']
        ]));
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function recommendation(Request $request, Company $company)
    {
        $validData = $this->validate($request, [
            'bool' => 'required|boolean'
        ]);

        return $company->recommendations()->save($rating = auth()->user()->recommendations()->make([
            'bool' => $validData['bool']
        ]));
    }
}
