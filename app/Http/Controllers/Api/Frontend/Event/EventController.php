<?php

namespace App\Http\Controllers\Api\Frontend\Event;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function feature(Request $request)
    {
        $validData = $this->validate($request, [
            'slug' => 'required|exists:events,slug'
        ]);

        $event = Event::where('slug', $validData['slug'])->firstOrFail();

        return $event->feature();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function rating(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'star' => 'required|numeric|between:1,5'
        ]);

        return $event->ratings()->save($rating = auth()->user()->ratings()->make([
            'star' => $validData['star']
        ]));
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function recommendation(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'bool' => 'required|boolean'
        ]);

        return $event->recommendations()->save($rating = auth()->user()->recommendations()->make([
            'bool' => $validData['bool']
        ]));
    }
}
