<?php

namespace App\Http\Controllers;

use App\EventQuestion;
use Illuminate\Http\Request;

class EventQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventQuestion  $eventQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(EventQuestion $eventQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventQuestion  $eventQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(EventQuestion $eventQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventQuestion  $eventQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventQuestion $eventQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventQuestion  $eventQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventQuestion $eventQuestion)
    {
        //
    }
}
