<?php

namespace App\Http\Controllers;

use App\EventRating;
use Illuminate\Http\Request;

class EventRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventRating  $eventRating
     * @return \Illuminate\Http\Response
     */
    public function show(EventRating $eventRating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventRating  $eventRating
     * @return \Illuminate\Http\Response
     */
    public function edit(EventRating $eventRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventRating  $eventRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventRating $eventRating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventRating  $eventRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventRating $eventRating)
    {
        //
    }
}
