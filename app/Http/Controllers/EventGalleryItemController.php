<?php

namespace App\Http\Controllers;

use App\EventGalleryItem;
use Illuminate\Http\Request;

class EventGalleryItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventGalleryItem  $eventGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function show(EventGalleryItem $eventGalleryItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventGalleryItem  $eventGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function edit(EventGalleryItem $eventGalleryItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventGalleryItem  $eventGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventGalleryItem $eventGalleryItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventGalleryItem  $eventGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventGalleryItem $eventGalleryItem)
    {
        //
    }
}
