<?php

namespace App\Http\Controllers\Frontend\Event;

use App\BannerPlace;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\City\Event\Category\CategoryController;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request)
    {
        return resolve(CategoryController::class)->show($request, city());
    }

    public function show(Request $request, Event $event)
    {
        $city = $city ?? city();

        $event->addView();
        $event->addRecentlyViewedList();

        return view('frontend.event.show')
            ->withEvent($event->load([
                'addresses',
                'company',
                'views_today',
                'categories',
                'comments',
                'comments.user',
                'comments.answers',
                'comments.answers.user',
                'questions',
                'questions.user',
                'questions.answers',
                'questions.answers.user',
                'gallery_items',
                'gallery_items.attachable',
                'labels',
                'rating_results'
            ]))
            ->withEventOnePlace(BannerPlace::where('key', 'event-one')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withEventSecondPlace(BannerPlace::where('key', 'event-second')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first());
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return false|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function review(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'thumb' => 'required|boolean',
            'rating' => 'required|between:1,5',
            'review' => 'required|string'
        ]);

        if (empty($event->user_rating)) {
            $event->ratings()->save($rating = auth()->user()->ratings()->make([
                'star' => $validData['rating']
            ]));
        }

        if (empty($event->user_recommendation)) {
            $event->recommendations()->save($rating = auth()->user()->recommendations()->make([
                'bool' => $validData['thumb']
            ]));
        }

        $event->comments()->save($comment = auth()->user()->comments()->make([
            'text' => $validData['review'],
            'published' => 1
        ]));

        return back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return false|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function question(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'review' => 'required|string'
        ]);

        $event->questions()->save($question = auth()->user()->questions()->make([
            'text' => $validData['review'],
            'published' => 1
        ]));

        return back();
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return false|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function report(Request $request, Event $event)
    {
        $validData = $this->validate($request, [
            'text' => 'nullable|string',
            'checkbox.*' => 'nullable|string',
        ]);

        $event->reports()->save($report = auth()->user()->reports()->make([
            'text' => $validData['text'] ?? '' . '; ' . implode('; ', $validData['checkbox'] ?? [])
        ]));

        return back();
    }
}
