<?php

namespace App\Http\Controllers\Frontend\Page;

use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    public function dynamic(Page $page)
    {
        return view('frontend.page.dynamic')
            ->withPage($page);
    }
}
