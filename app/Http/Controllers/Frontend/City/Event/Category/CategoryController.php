<?php

namespace App\Http\Controllers\Frontend\City\Event\Category;

use App\BannerPlace;
use App\City;
use App\Event;
use App\EventCategory as Category;
use App\Http\Controllers\Controller;
use GoogleMaps;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Request $request, City $city, Category $category = null)
    {
        // TODO встроить фильтры
        $query = Event::when($category, function ($q) use ($category) {
            $q->whereHas('categories', function ($q) use ($category) {
                $q->where('id', $category->id);
            });
        })
            ->whereHas('addresses', function ($q) use ($city) {
                $q->where('city_id', $city->id);
            })
            ->with('addresses')
            ->active();

        $categories = (clone $query)->has('categories')->with('categories')->get()->reduce(function ($carry, $item) {
            return $carry->merge($item->categories);
        }, collect());

        if ($request->input('items')) {
            $query->whereIn('id', explode(',', $request->input('items')));
        }

        if ($request->input('text')) {
            $query->where('name', 'LIKE', '%' . $request->input('text') . '%');
        }

        if ($request->input('favorite')) {
            $query->where('favorite', 1);
        }

        if ($request->input('date')) {
            if (!empty($dates = explode('_', $request->input('date')))) {
                $startTime = $dates[0];
                $endTime = $dates[1];
                $query->where(function ($query) use ($startTime, $endTime) {
                    $query
                        ->where(function ($query) use ($startTime, $endTime) {
                            $query
                                ->whereDate('start', '<=', $startTime)
                                ->whereDate('end', '>', $startTime);
                        })
                        ->orWhere(function ($query) use ($startTime, $endTime) {
                            $query
                                ->whereDate('start', '<', $endTime)
                                ->whereDate('end', '>=', $endTime);
                        })
                        ->orWhere(function ($query) use ($startTime, $endTime) {
                            $query
                                ->whereDate('start', '>=', $startTime)
                                ->whereDate('end', '<=', $endTime);
                        });
                });
            }
        }

        if ($request->input('categories') && is_array($request->input('categories'))) {
            $categoryIds = $request->input('categories');
            $query->whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn('id', $categoryIds);
            });
        }

        if ($request->input('location') && is_array($request->input('location')) && !empty($request->input('location.name')) && !empty($request->input('location.range'))) {
            $location = json_decode(resolve(GoogleMaps::class)->load('geocoding')
                ->setParam([
                    'address' => $request->input('location.name')
                ])
                ->get());
            // TODO привести в нормальный вид, оказывается гугл не всегда может дать кординаты
            if (count($location->results) > 0) {
                $location = $location->results[0]->geometry->location;
            } else {
                $api = new \Yandex\Geo\Api();
                $api->setQuery($request->input('location.name'));
                $api->setLimit(1)// кол-во результатов
                ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                ->load();
                $response = $api->getResponse();

                $location = new \stdClass();
                $location->lat = $response->getList()[0]->getLatitude();
                $location->lng = $response->getList()[0]->getLongitude();
            }
            $query->whereHas('addresses', function ($query) use ($request, $location) {
                $query->isWithinMaxDistance($location, $request->input('location.range') / 1000);
            });
        }

        // адреса для карты
        // TODO вынести в отдельный API, чтобы запрос на адреса посылался только при открытии карты
        $addresses = (clone $query)->with('addresses.events')->get()->reduce(function ($carry, $item) use ($city) {
            $add = $item->addresses->filter(function ($item) use ($city) {
                return $item->city_id == $city->id;
            });
            return $carry->merge($add);
        }, collect());

        // отсортированные эвенты для показа в списке
        if ($request->input('sort-raiting') || $request->input('sort-views')) {
            if ($request->input('sort-raiting')) {
                $query->orderBy('star', $request->input('sort-raiting'));
            } else if ($request->input('sort-views')) {
                $query->orderBy('views', $request->input('sort-views'));
            }
        } else {
            $query->orderBy('id', 'desc');
        }

        $events = (clone $query)->with([
            'labels',
            'categories',
            'company'
        ])
            ->paginate($request->input('pagination.per_page') ?? 32);

        return view('frontend.city.event.category.show')
            ->withCity($city)
            ->withCategory($category)
            ->withEvents($events)
            ->withAddresses($addresses)
            ->withCategories($categories)
            ->withEventListOnePlace(BannerPlace::where('key', 'event-list-one')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withEventListSecondPlace(BannerPlace::where('key', 'event-list-second')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withEventListTreePlace(BannerPlace::where('key', 'event-list-tree')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first());
    }
}
