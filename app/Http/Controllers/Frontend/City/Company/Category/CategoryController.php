<?php

namespace App\Http\Controllers\Frontend\City\Company\Category;

use App\BannerPlace;
use App\City;
use App\Company;
use App\CompanyCategory as Category;
use App\Http\Controllers\Controller;
use GoogleMaps;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Request $request, City $city, Category $category = null)
    {
        // TODO встроить фильтры
        $query = Company::when($category, function ($q) use ($category) {
            $q->whereHas('categories', function ($q) use ($category) {
                $q->where('id', $category->id);
            });
        })
            ->whereHas('addresses', function ($q) use ($city) {
                $q->where('city_id', $city->id);
            })
            ->with('addresses')
            ->active();

        $categories = (clone $query)->has('categories')->with('categories')->get()->reduce(function ($carry, $item) {
            return $carry->merge($item->categories);
        }, collect());

        if ($request->input('items')) {
            $query->whereIn('id', explode(',', $request->input('items')));
        }

        if ($request->input('text')) {
            $query->where('name', 'LIKE', '%' . $request->input('text') . '%');
        }

        if ($request->input('categories') && is_array($request->input('categories'))) {
            $categoryIds = $request->input('categories');
            $query->whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn('id', $categoryIds);
            });
        }

        if ($request->input('location') && is_array($request->input('location')) && !empty($request->input('location.name')) && !empty($request->input('location.range'))) {
            $location = json_decode(resolve(GoogleMaps::class)->load('geocoding')
                ->setParam([
                    'address' => $request->input('location.name')
                ])
                ->get())->results[0]->geometry->location;
            $query->whereHas('addresses', function ($query) use ($request, $location) {
                $query->isWithinMaxDistance($location, $request->input('location.range') / 1000);
            });
        }

        // отсортированные эвенты для показа в списке
        if ($request->input('sort-raiting')) {
            $query->orderBy('star', $request->input('sort-raiting'));
        } else {
            $query->orderBy('id', 'desc');
        }

        $companies = (clone $query)
            ->paginate($request->input('pagination.per_page') ?? 32);

        // TODO категории должны просчитываться на наличии в них активных компаний
        return view('frontend.city.company.category.show')
            ->withCity($city)
            ->withCategory($category)
            ->withCompanies($companies)
            ->withCategories($categories)
            ->withCompanyListOnePlace(BannerPlace::where('key', 'company-list-one')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withCompanyListSecondPlace(BannerPlace::where('key', 'company-list-second')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withCompanyListTreePlace(BannerPlace::where('key', 'company-list-tree')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first());
    }
}
