<?php

namespace App\Http\Controllers\Frontend\City;

use App\BannerPlace;
use App\Event;
use App\EventCategory;
use App\Http\Controllers\Controller;
use App\City;
use App\Slide;
use App\Tile;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function show(Request $request, City $city = null)
    {
        $city = $city ?? city();
        $eventCategories = EventCategory::whereHas('events', function ($q) use ($city) {
            $q->active()->whereHas('addresses', function ($q) use ($city) {
                $q->where('city_id', $city->id);
            });
        })->get();

        //события сегодня
        $eventsTodayQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active()
            ->whereDate('start', '<=', date('Y-m-d'))
            ->with([
                'labels',
                'categories',
                'company',
                'addresses'
            ]);
        $eventsTodayCount = (clone $eventsTodayQuery)->count();
        $eventsToday = (clone $eventsTodayQuery)->orderBy('id', 'desc')->limit(6)->get();

        //события рекомендуем
        $eventsFavoriteQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active()
            ->where('favorite', 1)
            ->with([
                'labels',
                'categories',
                'company',
                'addresses'
            ]);
        $eventsFavoriteCount = (clone $eventsFavoriteQuery)->count();
        $eventsFavorite = (clone $eventsFavoriteQuery)->orderBy('id', 'desc')->limit(6)->get();

        //события лучшие оценки
        $eventsStarQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active()
            ->where('star', '>', 0)
            ->with([
                'labels',
                'categories',
                'company',
                'addresses'
            ]);
        $eventsStarCount = (clone $eventsStarQuery)->count();
        $eventsStar = (clone $eventsStarQuery)->orderBy('star', 'desc')->limit(6)->get();

        //события популярные
        $eventsPopularQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active()
            ->with([
                'labels',
                'categories',
                'company',
                'addresses'
            ]);
        $eventsPopularCount = (clone $eventsPopularQuery)->count();
        $eventsPopular = (clone $eventsPopularQuery)->orderBy('views', 'desc')->limit(6)->get();

        //события все события портала
        $eventsAllQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active()
            ->with([
                'labels',
                'categories',
                'company',
                'addresses'
            ]);
        $eventsAllCount = (clone $eventsAllQuery)->count();
        $eventsAll = (clone $eventsAllQuery)->orderBy('id', 'desc')->limit(6)->get();

        return view('frontend.city.show')
            ->withCity($city)
            ->withEventCategories($eventCategories)
            ->withEventsTodayCount($eventsTodayCount)
            ->withEventsToday($eventsToday)
            ->withEventsFavoriteCount($eventsFavoriteCount)
            ->withEventsFavorite($eventsFavorite)
            ->withEventsStarCount($eventsStarCount)
            ->withEventsStar($eventsStar)
            ->withEventsPopular($eventsPopular)
            ->withEventsPopularCount($eventsPopularCount)
            ->withEventsAllCount($eventsAllCount)
            ->withEventsAll($eventsAll)
            ->withSlides(Slide::whereHas('city', function ($query) use ($city) {
                $query->where('id', $city->id);
            })->with('selection.events', 'selection.companies')->get())
            ->withTiles(Tile::whereHas('city', function ($query) use ($city) {
                $query->where('id', $city->id);
            })->with('selection.events', 'selection.companies')->get())
            ->withMainBannerPlace(BannerPlace::where('key', 'main-banner')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                },
                'activeBanners.download'
            ])->first());
    }
}
