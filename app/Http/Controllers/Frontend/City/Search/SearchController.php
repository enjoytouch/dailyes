<?php

namespace App\Http\Controllers\Frontend\City\Search;

use App\City;
use App\Company;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request, City $city)
    {
        // TODO встроить фильтры
        $eventsQuery = Event::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active();

        if ($request->text) {
            $eventsQuery->where('name', 'LIKE', '%' . $request->text . '%');
        }

        $eventsCount = (clone $eventsQuery)->count();

        $events = (clone $eventsQuery)->with([
            'addresses',
            'labels',
            'categories',
            'company'
        ])
            ->limit(8)->get();

        // TODO встроить фильтры
        $companiesQuery = Company::whereHas('addresses', function ($q) use ($city) {
            $q->where('city_id', $city->id);
        })
            ->active();

        if ($request->text) {
            $companiesQuery->where('name', 'LIKE', '%' . $request->text . '%');
        }

        $companiesCount = (clone $companiesQuery)->count();

        $companies = (clone $companiesQuery)->with([
            'addresses',
            'categories'
        ])
            ->limit(3)->get();

        return view('frontend.city.search.index')
            ->withEvents($events)
            ->withEventsCount($eventsCount)
            ->withCompanies($companies)
            ->withCompaniesCount($companiesCount);
    }
}
