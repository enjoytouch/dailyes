<?php

namespace App\Http\Controllers\Frontend\Company;

use App\BannerPlace;
use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\City\Company\Category\CategoryController;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        return resolve(CategoryController::class)->show($request, city());
    }

    public function show(Request $request, Company $company)
    {
        $city = $city ?? city();

        $company->addView();

        return view('frontend.company.show')
            ->withCompany($company->load([
                'addresses',
                'active_events',
                'completed_events',
                'views_today',
                'categories',
                'comments',
                'comments.user',
                'comments.answers',
                'comments.answers.user',
                'gallery_items',
                'gallery_items.attachable'
            ]))
            ->withCompanyOnePlace(BannerPlace::where('key', 'company-one')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first())
            ->withCompanySecondPlace(BannerPlace::where('key', 'company-second')->with([
                'activeBanners' => function ($query) use ($city) {
                    $query->whereHas('city', function ($query) use ($city) {
                        $query->where('id', $city->id);
                    });
                }, 'activeBanners.download'
            ])->first());
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return false|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function review(Request $request, Company $company)
    {
        $validData = $this->validate($request, [
            'thumb' => 'required|boolean',
            'rating' => 'required|between:1,5',
            'review' => 'required|string'
        ]);

        if (empty($company->user_rating)) {
            $company->ratings()->save($rating = auth()->user()->ratings()->make([
                'star' => $validData['rating']
            ]));
        }

        if (empty($company->user_recommendation)) {
            $company->recommendations()->save($rating = auth()->user()->recommendations()->make([
                'bool' => $validData['thumb']
            ]));
        }

        $company->comments()->save($comment = auth()->user()->comments()->make([
            'text' => $validData['review'],
            'published' => 1
        ]));

        return back();
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return false|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function report(Request $request, Company $company)
    {
        $validData = $this->validate($request, [
            'text' => 'nullable|string',
            'checkbox.*' => 'nullable|string',
        ]);

        $company->reports()->save($report = auth()->user()->reports()->make([
            'text' => $validData['text'] ?? '' . '; ' . implode('; ', $validData['checkbox'] ?? [])
        ]));

        return back();
    }
}
