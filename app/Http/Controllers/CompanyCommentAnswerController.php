<?php

namespace App\Http\Controllers;

use App\CompanyCommentAnswer;
use Illuminate\Http\Request;

class CompanyCommentAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyCommentAnswer  $companyCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyCommentAnswer $companyCommentAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyCommentAnswer  $companyCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyCommentAnswer $companyCommentAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyCommentAnswer  $companyCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyCommentAnswer $companyCommentAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyCommentAnswer  $companyCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyCommentAnswer $companyCommentAnswer)
    {
        //
    }
}
