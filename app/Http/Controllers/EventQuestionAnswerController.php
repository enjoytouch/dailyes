<?php

namespace App\Http\Controllers;

use App\EventQuestionAnswer;
use Illuminate\Http\Request;

class EventQuestionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventQuestionAnswer  $eventQuestionAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(EventQuestionAnswer $eventQuestionAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventQuestionAnswer  $eventQuestionAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(EventQuestionAnswer $eventQuestionAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventQuestionAnswer  $eventQuestionAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventQuestionAnswer $eventQuestionAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventQuestionAnswer  $eventQuestionAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventQuestionAnswer $eventQuestionAnswer)
    {
        //
    }
}
