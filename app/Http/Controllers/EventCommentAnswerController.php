<?php

namespace App\Http\Controllers;

use App\EventCommentAnswer;
use Illuminate\Http\Request;

class EventCommentAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventCommentAnswer  $eventCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(EventCommentAnswer $eventCommentAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCommentAnswer  $eventCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(EventCommentAnswer $eventCommentAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventCommentAnswer  $eventCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventCommentAnswer $eventCommentAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventCommentAnswer  $eventCommentAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventCommentAnswer $eventCommentAnswer)
    {
        //
    }
}
