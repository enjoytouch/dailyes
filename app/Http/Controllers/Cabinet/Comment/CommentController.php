<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Company;
use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \Auth::user();

        $companies = Company::has('comments')->with([
            'comments' => function ($query) use ($user) {
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('id', $user->id);
                });
            }
        ])->get();
        $events = Event::has('comments')->with([
            'comments' => function ($query) use ($user) {
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('id', $user->id);
                });
            }
        ])->get();

        return view('cabinet.comment.index')
            ->withCompanies($companies)
            ->withEvents($events);
    }

    public function show()
    {
        return view('cabinet.comment.show');
    }
}
