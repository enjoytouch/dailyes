<?php

namespace App\Http\Controllers\Cabinet\Event;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // TODO встроить фильтры
        $query = \Auth::user()->events()->with('addresses');

        if ($request->input('completed')) {
            $query->whereDate('end', '<', date('Y-m-d'));
        } else {
            $query->whereDate('end', '>=', date('Y-m-d'));
        }

        if ($request->text) {
            $query->where('name', 'LIKE', '%' . $request->text . '%');
        }

        // отсортированные эвенты для показа в списке
        $events = (clone $query)->orderBy($request->input('orderBy.column') ?? 'name', $request->input('orderBy.direction') ?? 'asc')
            ->with([
                'labels',
                'categories',
                'company'
            ])
            ->paginate($request->input('pagination.per_page') ?? 8);

        return view('cabinet.event.index')
            ->withEvents($events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cabinet.event.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('cabinet.event.edit')
            ->withEvent($event);
    }

    public function stat(Event $event)
    {
        return view('cabinet.event.stat')
            ->withEvent($event->load(['views_weekly']));
    }
}
