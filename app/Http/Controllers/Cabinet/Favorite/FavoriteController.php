<?php

namespace App\Http\Controllers\Cabinet\Favorite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cabinet.favorite.index')
            ->withEvents($events = \Auth::user()->favorite_events)
            ->withCompanies($companies = \Auth::user()->favorite_companies);
    }
}
