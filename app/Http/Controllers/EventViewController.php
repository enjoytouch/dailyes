<?php

namespace App\Http\Controllers;

use App\EventView;
use Illuminate\Http\Request;

class EventViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventView  $eventView
     * @return \Illuminate\Http\Response
     */
    public function show(EventView $eventView)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventView  $eventView
     * @return \Illuminate\Http\Response
     */
    public function edit(EventView $eventView)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventView  $eventView
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventView $eventView)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventView  $eventView
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventView $eventView)
    {
        //
    }
}
