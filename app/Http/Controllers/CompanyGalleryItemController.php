<?php

namespace App\Http\Controllers;

use App\CompanyGalleryItem;
use Illuminate\Http\Request;

class CompanyGalleryItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyGalleryItem  $companyGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyGalleryItem $companyGalleryItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyGalleryItem  $companyGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyGalleryItem $companyGalleryItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyGalleryItem  $companyGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyGalleryItem $companyGalleryItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyGalleryItem  $companyGalleryItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyGalleryItem $companyGalleryItem)
    {
        //
    }
}
