<?php

namespace App\Http\Controllers;

use App\CompanyRecomendation;
use Illuminate\Http\Request;

class CompanyRecomendationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyRecomendation  $companyRecomendation
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyRecomendation $companyRecomendation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyRecomendation  $companyRecomendation
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyRecomendation $companyRecomendation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyRecomendation  $companyRecomendation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyRecomendation $companyRecomendation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyRecomendation  $companyRecomendation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyRecomendation $companyRecomendation)
    {
        //
    }
}
