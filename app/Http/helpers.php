<?php

use App\City;
use App\Helpers\Frontend\Spravkur\Pular as PularHelper;
use Illuminate\Database\Query\Expression;

/*
 * Global helpers file with misc functions.
 */
if (! function_exists('include_route_files')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $it = new FilesystemIterator($folder);

            while ($it->valid()) {
                if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

if (!function_exists('city')) {

    function city()
    {
        return City::when($citySlug = request()->cookie('citySlug'), function ($q) use ($citySlug) {
            return $q->where('slug', $citySlug);
        })->first() ?? City::near()->firstOrFail();
    }
}

// пример форм ['отзыв', 'отзыва', 'отзывов']
if (! function_exists('pular')) {

    /**
     * @param int $number
     * @param array $forms
     * @return string
     */
    function pular(int $number, array $forms)
    {
        $number %= 100;
        if ($number >= 5 && $number <= 20) {
            return $forms[2];
        }
        $number %= 10;
        if ($number == 1) {
            return $forms[0];
        }
        if ($number >= 2 && $number <= 4) {
            return $forms[1];
        }
        return $forms[2];
    }
}

if (!function_exists('recently_events')) {

    function recently_events()
    {
        $events = json_decode(\Cookie::get('recently_viewed_events'), TRUE);
        if (empty($events)) {
            return collect();
        }
        krsort($events);

        return \App\Event::whereIn('id', $events)
            ->orderBy(new Expression('FIELD(id,' . implode(',', $events) . ')'))
            ->limit(6)
            ->get();
    }
}
