<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/backend/download',
        'api/backend/download/store-multiple',
        'api/backend/video/store-multiple',
        'api/backend/gallery-item/store-multiple',
        'api/cabinet/download',
        'api/cabinet/gallery-item/store-multiple',
    ];
}
