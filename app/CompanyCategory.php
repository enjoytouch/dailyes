<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Storage;

class CompanyCategory extends Model
{
    use Sluggable;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $fillable = ['name', 'slug', 'background_download_id'];

    protected $appends = ['background_url'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_has_company_categories');
    }

    public function background()
    {
        return $this->belongsTo(Download::class, 'background_download_id');
    }

    public function getBackgroundUrlAttribute()
    {
        if (empty($this->background)) {
            return 'http://via.placeholder.com/1440x750';
        }
        return Storage::url($this->background->path);
    }
}
