<?php

namespace App;

use App\Events\Rating\RatingCreated;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['star'];

    protected $dispatchesEvents = [
        'created' => RatingCreated::class
    ];

    public function estimated()
    {
        return $this->morphTo();
    }
}
