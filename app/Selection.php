<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selection extends Model
{
    protected $fillable = ['name', 'params'];

    protected $casts = [
        'params' => 'array'
    ];

    public function companies()
    {
        return $this->morphedByMany(Company::class, 'model', 'selection_has_models');
    }

    public function events()
    {
        return $this->morphedByMany(Event::class, 'model', 'selection_has_models');
    }
}
