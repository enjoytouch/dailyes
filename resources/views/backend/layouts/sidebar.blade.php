<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/admin/dashboard">
                    <i class="nav-icon icon-speedometer"></i> Панель управления
                </a>
            </li>
            <li class="nav-title">Настройки</li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Пользователи</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/users">
                            <i class="nav-icon icon-people"></i> Список юзеров
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/roles">
                            <i class="nav-icon icon-key"></i> Роли
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> События</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/event">
                            <i class="nav-icon icon-key"></i> Список событий
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/event/category">
                            <i class="nav-icon icon-key"></i> Категории событий
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/event/label">
                            <i class="nav-icon icon-key"></i> Лейблы событий
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Компании</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/company">
                            <i class="nav-icon icon-key"></i> Список компаний
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/company/category">
                            <i class="nav-icon icon-key"></i> Категории компаний
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Сборки</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/selection">
                            <i class="nav-icon icon-key"></i> Сборки
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/slide">
                            <i class="nav-icon icon-key"></i> Слайды
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/tile">
                            <i class="nav-icon icon-key"></i> Плитки
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Баннеры</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/banner">
                            <i class="nav-icon icon-key"></i> Список баннеров
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/banner/place">
                            <i class="nav-icon icon-key"></i> Места баннеров
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Отзывы</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/comment">
                            <i class="nav-icon icon-key"></i> Список отзывов
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/comment/answer">
                            <i class="nav-icon icon-key"></i> Список ответов
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Вопросы</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/question">
                            <i class="nav-icon icon-key"></i> Список вопросов
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/question/answer">
                            <i class="nav-icon icon-key"></i> Список ответов
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/report">
                    <i class="nav-icon icon-key"></i> Жалобы
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/city">
                    <i class="nav-icon icon-key"></i> Города
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/page">
                    <i class="nav-icon icon-key"></i> Страницы
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/subscriber">
                    <i class="nav-icon icon-key"></i> Подписчики
                </a>
            </li>
        </ul>
    </nav>
    <sidebar></sidebar>
</div>
