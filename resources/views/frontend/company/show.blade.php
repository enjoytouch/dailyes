@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.company')
@endsection

@push('after-scripts')
    <script src="/js/sticky.min.js"></script>
    <script>
        var sticky = new Sticky('.selector');
    </script>
    <script>
        $('#report4').click(function () {
            if ($(this).is(':checked')) {
                $('.report-textarea').show();
            } else {
                $('.report-textarea').hide();
            }
        });
    </script>
@endpush

@section('title', $company->name ?? '')
@section('description', $company->summary ?? '')

@section('content')
    <div class="container mw text-global-block">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="text-block">
                    <div class="text-block__title">
                        Описание
                    </div>
                    <div class="text-block__text">
                        {!! nl2br(e($company->about)) !!}
                    </div>
                    <div class="text-block__more">
                        <span>Читать полностью</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 text-center">
                <div class="text-block__ad-fixed mobhb">
                    @if($companyOnePlace->activeBanners->count() > 0)
                        <div id="owl-card-1" class="owl-carousel owl-theme" style="width: 240px; height: 400px; margin: 0px 0px 10px auto;">
                            @foreach($companyOnePlace->activeBanners as $activeBanner)
                                <a href="{{ $activeBanner->link }}" target="_blank">
                                    <img src="{{ $activeBanner->download->url }}" style="display: block; width: 100%;"/>
                                </a>
                            @endforeach
                        </div>
                    @else
                        <img src="/images/black-friday.png"/>
                    @endif
                    <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                </div>
                <div class="scroll_ad adhd">
                    <ul>
                        <li>
                            <div class="text-block__ad-fixed">
                                @if($companyOnePlace->activeBanners->count() > 0)
                                    <div id="owl-card-2" class="owl-carousel owl-theme">
                                        @foreach($companyOnePlace->activeBanners as $activeBanner)
                                            <a href="{{ $activeBanner->link }}" target="_blank">
                                                <img src="{{ $activeBanner->download->url }}"/>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <img src="/images/black-friday.png"/>
                                @endif
                                <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="text-block__ad-fixed">
                                @if($companySecondPlace->activeBanners->count() > 0)
                                    <div id="owl-card-3" class="owl-carousel owl-theme">
                                        @foreach($companySecondPlace->activeBanners as $activeBanner)
                                            <a href="{{ $activeBanner->link }}" target="_blank">
                                                <img src="{{ $activeBanner->download->url }}"/>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <img src="/images/black-friday.png"/>
                                @endif
                                <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                @if($company->gallery_items->count() > 0)
                    <div class="text-block__title">
                        Медиагалерея
                    </div>
                    <div class="mediagalleryMob">
                        <div id="owl-gallery" class="owl-carousel">
                            @foreach($company->gallery_items as $gallery_item)
                                @if($gallery_item->attachable_type == 'App\Video' || ($loop->iteration == 6 && $loop->remaining > 0))
                                    <div class="item">
                                        <div class="block-gallery">
                                            <img class="block-gallery__photo"
                                                 src="{{ $gallery_item->attachable->url }}"/>
                                            <a data-fancybox="gallery-mob"
                                               href="{{ $gallery_item->attachable_type == 'App\Video' ? $gallery_item->attachable->link : $gallery_item->attachable->url }}">
                                                <div class="block-gallery__shadow">
                                                    <div class="h-align">
                                                        <div class="v-align">
                                                            @if($loop->iteration == 6 && $loop->remaining > 0)
                                                                <div class="block-gallery__shadow-title">
                                                                    +{{ $loop->remaining }}</div>
                                                            @elseif($gallery_item->attachable_type == 'App\Video')
                                                                <img src="/images/icons/play-arrow.svg"/>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="item">
                                        <div class="block-gallery">
                                            <a data-fancybox="gallery-mob"
                                               href="{{ $gallery_item->attachable->url }}">
                                                <img class="block-gallery__photo"
                                                     src="{{ $gallery_item->attachable->url }}"/>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if($loop->iteration == 6)
                                    @break
                                @endif
                            @endforeach
                        </div>
                        <div hidden>
                            @foreach($company->gallery_items as $gallery_item)
                                @if($loop->iteration <= 6)
                                    @continue
                                @else
                                    @if($gallery_item->attachable_type == 'App\Video')
                                        <a data-fancybox="gallery-mob" href="{{ $gallery_item->attachable->link }}"></a>
                                    @elseif($gallery_item->attachable_type == 'App\Download')
                                        <a data-fancybox="gallery-mob"
                                           href="{{ $gallery_item->attachable->url }}"></a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="row block-gallery__global">
                        @foreach($company->gallery_items as $gallery_item)
                            @if($gallery_item->attachable_type == 'App\Video' || ($loop->iteration == 6 && $loop->remaining > 0))
                                <div class="col-md-4">
                                    <div class="block-gallery">
                                        <img class="block-gallery__photo" src="{{ $gallery_item->attachable->url }}"/>
                                        <a data-fancybox="gallery"
                                           href="{{ $gallery_item->attachable_type == 'App\Video' ? $gallery_item->attachable->link : $gallery_item->attachable->url }}">
                                            <div class="block-gallery__shadow">
                                                <div class="h-align">
                                                    <div class="v-align">
                                                        @if($loop->iteration == 6 && $loop->remaining > 0)
                                                            <div class="block-gallery__shadow-title">
                                                                +{{ $loop->remaining }}</div>
                                                        @elseif($gallery_item->attachable_type == 'App\Video')
                                                            <img src="/images/icons/play-arrow.svg"/>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-4">
                                    <div class="block-gallery">
                                        <a data-fancybox="gallery" href="{{ $gallery_item->attachable->url }}">
                                            <img class="block-gallery__photo"
                                                 src="{{ $gallery_item->attachable->url }}"/>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($loop->iteration == 6)
                                @break
                            @endif
                        @endforeach
                        <div hidden>
                            @foreach($company->gallery_items as $gallery_item)
                                @if($loop->iteration <= 6)
                                    @continue
                                @else
                                    @if($gallery_item->attachable_type == 'App\Video')
                                        <a data-fancybox="gallery" href="{{ $gallery_item->attachable->link }}"></a>
                                    @elseif($gallery_item->attachable_type == 'App\Download')
                                        <a data-fancybox="gallery"
                                           href="{{ $gallery_item->attachable->url }}"></a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="text-block__title">
                    Адреса компании на карте
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <addresses-carousel :addresses="{{ $company->addresses }}"></addresses-carousel>
                    </div>
                    <div class="col-md-12 paddMap">
                        <div id="mapList">
                            <addresses-map :addresses="{{ $company->addresses }}" :carousel="true"></addresses-map>
                        </div>
                    </div>
                </div>
                <div class="text-block__title">
                    Отзывы
                </div>
                <div class="reviews-rating">
                    <div class="reviews-rating__count">
                        <span>{{ round($company->star, 1) }}</span> / Из 5
                    </div>
                    <div class="reviews-rating__stars">
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-white"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label>
                    </div>
                    <div class="reviews-rating__progress">
                        @for ($i = 5; $i > 0; $i--)
                            <div class="v-align">
                                <div class="reviews-rating__progress-place">
                                    <div style="width: {{ isset($company->rating_results_array[$i]) ? round($company->rating_results_array[$i] / $company->count_stars * 100, 0) : 0 }}%"
                                         class="reviews-rating__progress-bar"></div>
                                </div>
                                <div class="reviews-rating__progress-count">{{ isset($company->rating_results_array[$i]) ? $company->rating_results_array[$i] : 0 }}</div>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="calc-block">
                    <div>Для расчета рейтинга используются общие оценки по характеристикам за все время</div>
                    <div>@if(!auth()->id())Войдите в свой профиль, что бы оставить отзыв, комментарий или задать
                        вопрос@endif</div>
                </div>
            </div>
            <div data-sticky-container class="col-md-3 text-center hide_block">
                <div class="text-block__ad selector" data-margin-top="20">
                    <div>
                        @if($companySecondPlace->activeBanners->count() > 0)
                            <div id="owl-card-4" class="owl-carousel owl-theme" style="width: 240px; height: 400px; margin: 0px 0px 10px auto;">
                                @foreach($companySecondPlace->activeBanners as $activeBanner)
                                    <a href="{{ $activeBanner->link }}" target="_blank">
                                        <img src="{{ $activeBanner->download->url }}"/>
                                    </a>
                                @endforeach
                            </div>
                        @else
                            <img src="/images/black-friday.png"/>
                        @endif
                        <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                    </div>
                </div>
            </div>
            @if(auth()->id() && !$company->comments->where('text', '!=', null)->contains('user_id', auth()->id()))
                <div class="col-md-12">
                    <button class="write_review">Написать отзыв</button>
                    <div class="onwrite-review">
                        <div class="v-align">
                            <div class="onwrite-review__avatar">
                                <img src="{{ auth()->user()->avatar_url }}"/>
                            </div>
                            <div class="onwrite-review__title">
                                {{ auth()->user()->name }}<br>
                                <span>Пользователь</span>
                            </div>
                        </div>
                        <form action="{{ route('frontend.company.review', $company) }}" method="post">
                            {{ csrf_field() }}
                            <div class="v-align">
                                <div class="onwrite-review__nav">
                                    @if($company->user_rating)
                                        <input type="hidden" name="rating" value="{{ $company->user_rating->star }}">
                                    @else
                                        <span>Поставьте оценку</span>
                                        <div class="onwrite-review__rating">
                                            <div class="onwrite-review__rating-wrap">
                                                <input class="onwrite-review__rating-input" id="star-review-5"
                                                       type="radio"
                                                       name="rating" value="5">
                                                <label class="star-review__ico" for="star-review-5"
                                                       title="5 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-4"
                                                       type="radio"
                                                       name="rating" value="4">
                                                <label class="star-review__ico" for="star-review-4"
                                                       title="4 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-3"
                                                       type="radio"
                                                       name="rating" value="3">
                                                <label class="star-review__ico" for="star-review-3"
                                                       title="3 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-2"
                                                       type="radio"
                                                       name="rating" value="2">
                                                <label class="star-review__ico" for="star-review-2"
                                                       title="2 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-1"
                                                       type="radio"
                                                       name="rating" value="1">
                                                <label class="star-review__ico" for="star-review-1"
                                                       title="1 из 5 звёзд"></label>
                                            </div>
                                        </div>
                                    @endif
                                    @if($company->user_recommendation)
                                        <input type="hidden" name="thumb"
                                               value="{{ $company->user_recommendation->bool }}">
                                    @else
                                        <span>Рекомендуете?</span>
                                        <div class="onwrite-review__blockmob">
                                            <input class="review__thumb-input up" id="thumb-review-2" type="radio"
                                                   name="thumb"
                                                   value="1">
                                            <label class="thumb-review__ico up" for="thumb-review-2"
                                                   title="Нравится"></label>
                                            <input class="review__thumb-input down" id="thumb-review-1" type="radio"
                                                   name="thumb" value="0">
                                            <label class="thumb-review__ico down" for="thumb-review-1"
                                                   title="Не нравится"></label>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <textarea name="review" placeholder="Напишите здесь отзыв"></textarea>
                            <div class="v-align">
                                <div class="onwrite-review__button">
                                    <button type="submit">Опубликовать</button>
                                    <span class="closeReview">Отменить</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <div class="col-md-12 review-close">
                @foreach($company->comments as $comment)
                    <div class="review">
                        <div class="review-block">
                            <div class="review-block__fixed">
                                <div class="v-align">
                                    @if($comment->rec)
                                        <div class="review-block__fixed-thumb-up"></div><span>Рекомендую</span>@endif
                                    <div class="review-block__fixed-star"></div>
                                    <span>{{ $comment->star }}</span>
                                </div>
                            </div>
                            <div class="v-align">
                                <div class="review-block__avatar">
                                    <img src="{{ $comment->user->avatar_url }}"/>
                                </div>
                                <div class="review-block__title">
                                    {{ $comment->user->name }}<br>
                                    <span>Дата отзыва: {{ $comment->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}</span>
                                </div>
                            </div>
                            <div class="review-block__text">
                                {{ $comment->text }}
                            </div>
                            <div class="review-block__nav">
                                <div class="v-align"><span class="review-block__nav-more">Отзыв полностью</span>
                                    <div class="review-block__nav-comments openComments">{{ $comment->answers->count() }}</div>
                                </div>
                            </div>
                        </div>
                        @if($comment->answers->count() > 0)
                            <div class="review-block__comments">
                                <div class="v-align">
                                    <span class="review-block__comments-count">{{ $comment->answers->count() }} {{ pular($comment->answers->count(), ['комментарий', 'комментария', 'комментариев']) }}</span>
                                    <span class="review-block__comments-hide">Свернуть комментарии <i
                                                class="fas fa-chevron-up"></i></span>
                                </div>
                                <div class="review-block__comments-row">
                                    @foreach($comment->answers as $answer)
                                        <div class="review-block__comments-list">
                                            <div class="v-align">
                                                <div class="review-block__comments-list-avatar">
                                                    <img src="{{ $answer->user->avatar_url }}"/>
                                                </div>
                                                <div class="review-block__comments-list-title">
                                                    @if($answer->user->hasRole('admin'))
                                                        Администратор@else{{ $answer->user->name }}@endif<br>
                                                    <span>Дата: {{ $answer->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}@if($answer->user->hasRole('admin'))
                                                            (администратор) @endif</span>
                                                </div>
                                            </div>
                                            <div class="review-block__comments-list-text">
                                                {{ $answer->text }}
                                            </div>
                                            {{--@if(auth()->id() && auth()->id() != $answer->user->id)--}}
                                            {{--<div class="review-block__comments-list-reply">--}}
                                            {{--<span>Ответить</span>--}}
                                            {{--</div>--}}
                                            {{--@endif--}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="col-md-12">
                <button class="more_review">Показать еще отзывы</button>
            </div>
            <div class="col-md-12">
                <div class="attract-block">
                    <div class="v-align">
                        <div class="attract-block__text">
                            Узнайте как привлечь новых клиентов<br>
                            <span>с помощью нашего сервиса</span><br>
                            <a href="/for-your-business" class="attract-block__button hid">
                                Подробнее
                            </a>
                        </div>
                        <a href="/for-your-business" class="attract-block__button">
                            Подробнее
                        </a>
                    </div>
                    <div class="attract-block__image">
                        <img src="/images/attract.png"/>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="similar-title">
                    Cобытия компании
                    <ul class="nav similar-title__group">
                        <li class="active similar-active">
                            <a data-toggle="tab" class="active" href="#active">Активные</a>
                        </li>
                        <li class="similar-completed">
                            <a data-toggle="tab" href="#completed">Завершенные</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="active" class="tab-pane in active show">
                        <div class="owl-carousel events-block gallery_owl">
                            @foreach($company->active_events as $active_event)
                                <div>
                                    <div class="events-block__image">
                                        <a href="{{ route('frontend.event.show', $active_event) }}"><img
                                                    src="{{ $active_event->image_url }}"/></a>
                                        @if($active_event->favorite)
                                            <div class="events-block__cool">
                                                <img src="/images/icons/success.svg"/> Выбор редакции
                                            </div>
                                        @endif
                                        <div class="events-block__badges">
                                            @foreach($active_event->labels as $label)
                                                <div class="events-block__badges-group">
                                                    <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                                    {{ $label->name }}
                                                </div>
                                            @endforeach
                                        </div>
                                        <bookmark :event="{{ json_encode($active_event->only('slug')) }}"
                                                  :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $active_event->id) : false) }}"></bookmark>
                                    </div>
                                    <div class="events-block__title">
                                        <a href="{{ route('frontend.event.show', $active_event) }}">{{ $active_event->name }}</a>
                                    </div>
                                    <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $active_event->categories[0] : $category]) }}"
                                       class="events-block__group-a">
                                        <span class="events-block__group">{{ empty($category) ? $active_event->categories[0]->name : $category->name }}</span>
                                    </a>
                                    <div class="events-block__text-desc">
                                        <a href="{{ route('frontend.company.show', $active_event->company) }}">{{ $active_event->company->name }}</a>
                                    </div>
                                    <div class="events-block__text-desc">
                                        @if($active_event->status == 'active')
                                            Дата
                                            окончания: {{ $active_event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($active_event->status == 'before')
                                            Дата
                                            начала: {{ $active_event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($active_event->status == 'after')
                                            Завершено
                                        @endif
                                    </div>
                                    <div class="events-block__text-icons v-align">
                                        <img src="/images/icons/thumb-up-blue.svg"/> {{ $active_event->rec }}%
                                        <img src="/images/icons/star-blue.svg"/> {{ round($active_event->star, 1) }}
                                        <img src="/images/icons/eye.svg"/> {{ $active_event->views }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="completed" class="tab-pane">
                        @if(count($company->completed_events) > 0)
                            <div class="owl-carousel events-block gallery_owl">
                                @foreach($company->completed_events as $completed_event)
                                    <div>
                                        <div class="events-block__ended">
                                            <a href="{{ route('frontend.event.show', $completed_event) }}">
                                                <div class="events-block__empty center">Событие завершено</div>
                                            </a>
                                            <img src="{{ $completed_event->image_url }}"/>
                                        </div>
                                        <div class="events-block__title">
                                            <a href="{{ route('frontend.event.show', $completed_event) }}">{{ $completed_event->name }}</a>
                                        </div>
                                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $completed_event->categories[0] : $category]) }}"
                                           class="events-block__group-a">
                                            <span class="events-block__group">{{ empty($category) ? $completed_event->categories[0]->name : $category->name }}</span>
                                        </a>
                                        <div class="events-block__text-desc">
                                            <a href="{{ route('frontend.company.show', $completed_event->company) }}">{{ $completed_event->company->name }}</a>
                                        </div>
                                        <div class="events-block__text-desc">
                                            @if($completed_event->status == 'active')
                                                Дата
                                                окончания: {{ $completed_event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                            @elseif($completed_event->status == 'before')
                                                Дата
                                                начала: {{ $completed_event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                            @elseif($completed_event->status == 'after')
                                                Завершено
                                            @endif
                                        </div>
                                        <div class="events-block__text-icons v-align">
                                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $completed_event->rec }}%
                                            <img src="/images/icons/star-blue.svg"/> {{ round($completed_event->star, 1) }}
                                            <img src="/images/icons/eye.svg"/> {{ $completed_event->views }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="report-panel">
        <a class="auth-panel__close reportPanel"><img src="/images/icons/close.svg"/></a>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="report-panel__title">Почему вы жалуетесь на эту компанию?</span>
                </div>
                <form id="respform" action="{{ route('frontend.company.report', $company) }}" method="post" style="margin-top: 70px"
                      class="col-md-12">
                    {{ csrf_field() }}
                    <div>
                        <input name="checkbox[0]" value="В описании указана не верная информация" type="checkbox"
                               id="report1" class="report-checkbox"/>
                        <label for="report1" class="label-report"></label>
                        <label for="report1" class="report-checkbox-text">В описании указана не верная
                            информация</label>
                    </div>
                    <div>
                        <input name="checkbox[1]" value="Фотографии не соответствуют действительности" type="checkbox"
                               id="report2" class="report-checkbox"/>
                        <label for="report2" class="label-report"></label>
                        <label for="report2" class="report-checkbox-text">Фотографии не соответствуют
                            действительности</label>
                    </div>
                    <div>
                        <input name="checkbox[2]" value="Компания не существует" type="checkbox" id="report3"
                               class="report-checkbox"/>
                        <label for="report3" class="label-report"></label>
                        <label for="report3" class="report-checkbox-text">Компания не существует</label>
                    </div>
                    <div>
                        <input type="checkbox" id="report4" class="report-checkbox"/>
                        <label for="report4" class="label-report"></label>
                        <label for="report4" class="report-checkbox-text">Проблема в другом</label>
                    </div>
                    <div class="report-textarea">
                        <textarea name="text" class="report-area"></textarea><br>
                    </div>
                    <div>
                        <button type="submit" class="report-submit">Отправить</button>
                    </div>
                </form>
                <h3 class="message-resp">Спасибо, что поделились данной информацией!<br>В ближайшее время мы проверим эту компанию.<br>
                    <button onclick="$('.reportPanel').click();" class="response-button">Хорошо</button>
                </h3>
            </div>
        </div>
    </div>
    <div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="map-content">
                <div class="map-content__header">
                    <span>События на карте</span>
                    <div class="map-content__header-close" data-dismiss="modal" aria-label="Close"><img
                                src="/images/icons/close.svg"/></div>
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 100%; height: 600px">
                        <addresses-map :addresses="{{ $company->addresses }}" :modal="true"></addresses-map>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
