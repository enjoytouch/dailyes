@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.city')
@endsection

@section('title', config('app.name') . ' в городе ' . $city->name ?? '')
@section('description', config('app.name') . ' в городе ' . $city->name ?? '')

@section('content')
    <div class="oh container-fluid">
        <div class="container minus-15">
            <div class="title-catalog">Категории событий</div>
            <div id="owl-1" class="owl-carousel events-block">
                @foreach($eventCategories as $eventCategory)
                    <a href="{{ route('frontend.city.event.category.show', [$city, $eventCategory]) }}">
                        <div class="events-block__item">
                            <img src="{{ $eventCategory->image_url }}"/>
                            <div class="events-block__text">
                                {{ $eventCategory->name }}
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="row">
            <div id="owl-2" class="owl-carousel rating-block owl-theme">
                @foreach($slides as $slide)
                    <div class="rating-block__item">
                        <img src="{{ $slide->image_url }}"/>
                        <div class="rating-block__content">
                            <div class="container">
                                <div class="rating-block__content-block">
                                    <div class="rating-block__content-title">
                                        {{ $slide->name }}
                                    </div>
                                    <div class="rating-block__content-desc">
                                        {!! nl2br(e($slide->summary)) !!}
                                    </div>
                                    <a href="{{ $slide->url }}">
                                        <div class="rating-block__content-button">
                                            {{ $slide->button }}
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container inter-blog">
            <div class="row">
                <div id="owl-3" class="owl-carousel">
                    @foreach($tiles as $tile)
                        <div>
                            <a href="{{ $tile->url }}">
                                <div class="inter-blog__item">
                                    <div class="inter-blog__item-title">{{ $tile->name }}</div>
                                    <div class="inter-blog__item-desc">{!! nl2br(e($tile->summary)) !!}
                                    </div>
                                    <div class="inter-blog__item-button"><img src="/images/icons/open_button.svg"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="container minus-15">
            <div class="title-more md-margin">
                <div class="title-more__title title-today">
                    События сегодня
                    <span class="title-more__all">{{ $eventsTodayCount }} {{ pular($eventsTodayCount, ['событие', 'события', 'событий']) }}</span>
                </div>
            </div>
            <div>
                <div class="title-more__container">
                    <div class="title-more__desc">Не откладывай на завтра</div>
                    <div class="title-more__view"><a
                                href="{{ route('frontend.event.index', ['date' => date('Y-m-d') . '_' . date('Y-m-d')]) }}">Показать
                            еще</a></div>
                </div>
            </div>
            <div class="owl-carousel events-block gallery_owl">
                @foreach($eventsToday as $event)
                    <div>
                        <div class="events-block__image">
                            <a href="{{ route('frontend.event.show', $event) }}">
                                <div class="events-block__empty"></div>
                            </a>
                            <img src="{{ $event->image_url }}"/>
                            @if($event->favorite)
                                <div class="events-block__cool">
                                    <img src="/images/icons/success.svg"/> Выбор редакции
                                </div>
                            @endif
                            <div class="events-block__badges">
                                @foreach($event->labels as $label)
                                    <div class="events-block__badges-group">
                                        <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                        {{ $label->name }}
                                    </div>
                                @endforeach
                            </div>
                            <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                      :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        </div>
                        <div class="events-block__title">
                            <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                        </div>
                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                           class="events-block__group-a">
                            <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                        </a>
                        <div class="events-block__text-desc">
                            <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                        </div>
                        <div class="events-block__text-desc">
                            @if($event->status == 'active')
                                Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'before')
                                Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'after')
                                Завершено
                            @endif
                        </div>
                        <div class="events-block__text-icons">
                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                            <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                            <img src="/images/icons/eye.svg"/> {{ $event->views }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 bnp">
                <a class="show-more-button-mob"
                   href="{{ route('frontend.event.index', ['date' => date('Y-m-d') . '_' . date('Y-m-d')]) }}">
                    <button>Показать еще</button>
                </a>
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="container minus-15">
            <div class="title-more">
                <div class="title-more__title title-index">
                    Cобытия рекомендуем
                    <span class="title-more__all">{{ $eventsFavoriteCount }} {{ pular($eventsFavoriteCount, ['событие', 'события', 'событий']) }}</span>
                </div>
            </div>
            <div>
                <div class="title-more__container">
                    <div class="title-more__desc">То, на что мы обратили свое внимание</div>
                    <div class="title-more__view"><a href="{{ route('frontend.event.index', ['favorite' => 1]) }}">Показать
                            еще</a></div>
                </div>
            </div>
            <div class="owl-carousel events-block gallery_owl">
                @foreach($eventsFavorite as $event)
                    <div>
                        <div class="events-block__image">
                            <a href="{{ route('frontend.event.show', $event) }}">
                                <div class="events-block__empty"></div>
                            </a>
                            <img src="{{ $event->image_url }}"/>
                            @if($event->favorite)
                                <div class="events-block__cool">
                                    <img src="/images/icons/success.svg"/> Выбор редакции
                                </div>
                            @endif
                            <div class="events-block__badges">
                                @foreach($event->labels as $label)
                                    <div class="events-block__badges-group">
                                        <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                        {{ $label->name }}
                                    </div>
                                @endforeach
                            </div>
                            <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                      :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        </div>
                        <div class="events-block__title">
                            <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                        </div>
                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                           class="events-block__group-a">
                            <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                        </a>
                        <div class="events-block__text-desc">
                            <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                        </div>
                        <div class="events-block__text-desc">
                            @if($event->status == 'active')
                                Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'before')
                                Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'after')
                                Завершено
                            @endif
                        </div>
                        <div class="events-block__text-icons">
                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                            <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                            <img src="/images/icons/eye.svg"/> {{ $event->views }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 bnp">
                <a class="show-more-button-mob" href="{{ route('frontend.event.index', ['favorite' => 1]) }}">
                    <button>Показать еще</button>
                </a>
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="container">
            <div class="title-more">
                <div class="title-more__title title-index">
                    Лучшие оценки
                    <span class="title-more__all">{{ $eventsStarCount }} {{ pular($eventsStarCount, ['событие', 'события', 'событий']) }}</span>
                </div>
            </div>
            <div>
                <div class="title-more__container">
                    <div class="title-more__desc">Основываясь на положительных отзывах пользователей портала</div>
                    <div class="title-more__view"><a
                                href="{{ route('frontend.event.index', ['sort-rating' => 'desc']) }}">Показать еще</a>
                    </div>
                </div>
            </div>
            <div class="owl-carousel events-block gallery_owl">
                @foreach($eventsStar as $event)
                    <div>
                        <div class="events-block__image">
                            <a href="{{ route('frontend.event.show', $event) }}">
                                <div class="events-block__empty"></div>
                            </a>
                            <img src="{{ $event->image_url }}"/>
                            @if($event->favorite)
                                <div class="events-block__cool">
                                    <img src="/images/icons/success.svg"/> Выбор редакции
                                </div>
                            @endif
                            <div class="events-block__badges">
                                @foreach($event->labels as $label)
                                    <div class="events-block__badges-group">
                                        <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                        {{ $label->name }}
                                    </div>
                                @endforeach
                            </div>
                            <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                      :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        </div>
                        <div class="events-block__title">
                            <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                        </div>
                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                           class="events-block__group-a">
                            <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                        </a>
                        <div class="events-block__text-desc">
                            <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                        </div>
                        <div class="events-block__text-desc">
                            @if($event->status == 'active')
                                Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'before')
                                Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'after')
                                Завершено
                            @endif
                        </div>
                        <div class="events-block__text-icons">
                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                            <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                            <img src="/images/icons/eye.svg"/> {{ $event->views }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 bnp">
                <a class="show-more-button-mob" href="{{ route('frontend.event.index', ['sort-rating' => 'desc']) }}">
                    <button>Показать еще</button>
                </a>
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="container">
            <div class="title-more">
                <div class="title-more__title title-index">
                    Популярные события
                    <span class="title-more__all">{{ $eventsPopularCount }} {{ pular($eventsPopularCount, ['событие', 'события', 'событий']) }}</span>
                </div>
            </div>
            <div>
                <div class="title-more__container">
                    <div class="title-more__desc">Мы выбрали для вас популярные себытия на текущий день</div>
                    <div class="title-more__view"><a
                                href="{{ route('frontend.event.index', ['sort-views' => 'desc']) }}">Показать еще</a>
                    </div>
                </div>
            </div>
            <div class="owl-carousel events-block gallery_owl">
                @foreach($eventsPopular as $event)
                    <div>
                        <div class="events-block__image">
                            <a href="{{ route('frontend.event.show', $event) }}">
                                <div class="events-block__empty"></div>
                            </a>
                            <img src="{{ $event->image_url }}"/>
                            @if($event->favorite)
                                <div class="events-block__cool">
                                    <img src="/images/icons/success.svg"/> Выбор редакции
                                </div>
                            @endif
                            <div class="events-block__badges">
                                @foreach($event->labels as $label)
                                    <div class="events-block__badges-group">
                                        <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                        {{ $label->name }}
                                    </div>
                                @endforeach
                            </div>
                            <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                      :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        </div>
                        <div class="events-block__title">
                            <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                        </div>
                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                           class="events-block__group-a">
                            <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                        </a>
                        <div class="events-block__text-desc">
                            <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                        </div>
                        <div class="events-block__text-desc">
                            @if($event->status == 'active')
                                Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'before')
                                Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'after')
                                Завершено
                            @endif
                        </div>
                        <div class="events-block__text-icons">
                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                            <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                            <img src="/images/icons/eye.svg"/> {{ $event->views }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 bnp">
                <a class="show-more-button-mob" href="{{ route('frontend.event.index', ['sort-views' => 'desc']) }}">
                    <button>Показать еще</button>
                </a>
            </div>
        </div>
    </div>
    <div class="oh container-fluid">
        <div class="container minus-15">
            <div class="title-more">
                <div class="title-more__title title-index">
                    Все события портала
                    <span class="title-more__all">{{ $eventsAllCount }} {{ pular($eventsAllCount, ['событие', 'события', 'событий']) }}</span>
                </div>
            </div>
            <div>
                <div class="title-more__container">
                    <div class="title-more__desc">Вы точно найдете здесь что то интересное</div>
                    <div class="title-more__view"><a href="{{ route('frontend.event.index') }}">Показать еще</a></div>
                </div>
            </div>
            <div class="owl-carousel events-block gallery_owl">
                @foreach($eventsAll as $event)
                    <div>
                        <div class="events-block__image">
                            <a href="{{ route('frontend.event.show', $event) }}">
                                <div class="events-block__empty"></div>
                            </a>
                            <img src="{{ $event->image_url }}"/>
                            @if($event->favorite)
                                <div class="events-block__cool">
                                    <img src="/images/icons/success.svg"/> Выбор редакции
                                </div>
                            @endif
                            <div class="events-block__badges">
                                @foreach($event->labels as $label)
                                    <div class="events-block__badges-group">
                                        <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                        {{ $label->name }}
                                    </div>
                                @endforeach
                            </div>
                            <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                      :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        </div>
                        <div class="events-block__title">
                            <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                        </div>
                        <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                           class="events-block__group-a">
                            <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                        </a>
                        <div class="events-block__text-desc">
                            <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                        </div>
                        <div class="events-block__text-desc">
                            @if($event->status == 'active')
                                Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'before')
                                Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                            @elseif($event->status == 'after')
                                Завершено
                            @endif
                        </div>
                        <div class="events-block__text-icons">
                            <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                            <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                            <img src="/images/icons/eye.svg"/> {{ $event->views }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 bnp">
                <a class="show-more-button-mob" href="{{ route('frontend.event.index') }}">
                    <button>Показать еще</button>
                </a>
            </div>
        </div>
    </div>
    @if($mainBannerPlace->activeBanners->count() > 0)
        <div id="owl-banner-main" class="container owl-carousel owl-theme">
            @foreach($mainBannerPlace->activeBanners as $activeBanner)
                <a href="{{ $activeBanner->link }}" target="_blank">
                    <img class="banner" style="width: 100%;" src="{{ $activeBanner->download->url }}"/>
                </a>
            @endforeach
        </div>
    @endif
@endsection
