@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.search')
@endsection

@push('after-scripts')
@endpush

@section('title', 'Результаты поиска')
@section('description', 'Результаты поиска')

@section('content')
    <div class="container-fluid search-panel">
        <div class="container minus-15">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-panel__title">
                        Найдено событий: <span>{{ $eventsCount }}</span>
                    </div>
                    <div class="search-panel__row events-block">
                        <div class="row bootfix-row">
                            @foreach($events as $event)
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 bootfix-col">
                                <div class="events-block__image">
                                    <a href="{{ route('frontend.event.show', $event) }}"><img
                                                src="{{ $event->image_url }}"/></a>
                                    @if($event->favorite)
                                        <div class="events-block__cool">
                                            <img src="/images/icons/success.svg"/> Выбор редакции
                                        </div>
                                    @endif
                                    <div class="events-block__badges">
                                        @foreach($event->labels as $label)
                                            <div class="events-block__badges-group">
                                                <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                                {{ $label->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                    <bookmark :event="{{ json_encode($event->only('slug')) }}" :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                                </div>
                                <div class="events-block__title">
                                    <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                                </div>
                                <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                                   class="events-block__group-a">
                                    <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                                </a>
                                <div class="events-block__text-desc">
                                    <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                                </div>
                                <div class="events-block__text-desc">
                                    @if($event->status == 'active')
                                        Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                    @elseif($event->status == 'before')
                                        Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                    @elseif($event->status == 'after')
                                        Завершено
                                    @endif
                                </div>
                                <div class="events-block__text-icons v-align">
                                    <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                                    <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                                    <img src="/images/icons/eye.svg"/> {{ $event->views }}
                                </div>
                            </div>
                            @endforeach
                            <div class="col-md-12 text-center bootfix-col">
                                <a class="search-panel__row-more event">Еще результаты по событиям <img src="/images/icons/more-arrow.svg" /><img src="/images/icons/more-arrow-blue.svg" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="search-panel__title">
                        Найдено компаний: <span>{{ $companiesCount }}</span>
                    </div>
                    <div class="row search-panel__company bootfix-row">
                        @foreach($companies as $company)
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 bootfix-col">
                            <div class="search-panel__company-block">
                                <img src="{{ $company->image_url }}" />
                                <a href="{{ route('frontend.company.show', $company) }}">
                                    <div class="events-block__empty"></div>
                                </a>
                                <a href="{{ route('frontend.city.company.category.show', [$city, empty($category) ? $company->categories[0] : $category]) }}">
                                    <div class="search-panel__company-group">
                                        {{ empty($category) ? $company->categories[0]->name : $category->name }}
                                    </div>
                                </a>
                                <company-bookmark :company="{{ json_encode($company->only('slug')) }}" :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_companies->contains('id', $company->id) : false) }}"></company-bookmark>
                                <div class="search-panel__company-text">
                                    <div class="search-panel__company-title">{{ $company->name }}</div>
                                    <div class="search-panel__company-place">{{ $company->addresses->count() > 0 ? $company->addresses[0]->address : '' }} {{ $company->addresses->count() > 1 ? ' и еще ' . ($company->addresses->count() - 1) : '' }}</div>
                                    <div class="search-panel__company-badges">
                                        <img src="/images/icons/thumb-up-black.svg" /> <span>{{ $company->rec }}</span>
                                        <img class="star" src="/images/icons/star.svg" /> <span>{{ round($company->star, 1) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-md-12 text-center bootfix-col">
                            <a class="search-panel__row-more company">Еще результаты по событиям <img src="/images/icons/more-arrow.svg" /><img src="/images/icons/more-arrow-blue.svg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
