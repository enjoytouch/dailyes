@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.company-category')
@endsection

@push('after-scripts')
    <script>
        $('.companies-container').infiniteScroll({
            path: '.pagination__next',
            append: '.company-item',
            button: '.view-more-button',
            // using button, disable loading on scroll
            scrollThreshold: false,
            // status: '.page-load-status',
        });
    </script>
@endpush

@section('title', 'Каталог компаний' . (empty($category) ? '' : ' - ' . $category->name))
@section('description', 'Каталог компаний' . (empty($category) ? '' : ' - ' . $category->name))

@section('content')
    <div class="container-fluid search-panel">
        <div class="container minus-15">
            <div class="row">
                <div class="col-md-12">
                    <div class="events-block company-block">
                        <div class="row companies-container bootfix-row">
                            @foreach($companies as $key => $company)
                                <div class="col-lg-3 col-md-6 col-sm-6 company-item bootfix-col">
                                    <div class="search-panel__company-block">
                                        <img src="{{ $company->image_url }}"/>
                                        <a href="{{ route('frontend.company.show', $company) }}">
                                            <div class="events-block__empty"></div>
                                        </a>
                                        <a href="{{ route('frontend.city.company.category.show', [$city, empty($category) ? $company->categories[0] : $category]) }}">
                                            <div class="search-panel__company-group">
                                                {{ empty($category) ? $company->categories[0]->name : $category->name }}
                                            </div>
                                        </a>
                                        <company-bookmark :company="{{ json_encode($company->only('slug')) }}"
                                                          :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_companies->contains('id', $company->id) : false) }}"></company-bookmark>
                                        <div class="search-panel__company-text">
                                            <div class="search-panel__company-title">{{ $company->name }}</div>
                                            <div class="search-panel__company-place">{{ $company->addresses->count() > 0 ? $company->addresses[0]->address : '' }} {{ $company->addresses->count() > 1 ? ' и еще ' . ($company->addresses->count() - 1) : '' }}</div>
                                            <div class="search-panel__company-badges">
                                                <img src="/images/icons/thumb-up-black.svg"/>
                                                <span>{{ $company->rec }}</span>
                                                <img class="star" src="/images/icons/star.svg"/>
                                                <span>{{ round($company->star, 1) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(($key + 1) == 8)
                                    @if($companyListOnePlace->activeBanners->count() > 0)
                                        <div id="owl-company-list-1" class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($companyListOnePlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @elseif(($key + 1) == 16)
                                    @if($companyListSecondPlace->activeBanners->count() > 0)
                                        <div id="owl-company-list-2" class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($companyListSecondPlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @elseif(($key + 1) == 32)
                                    @if($companyListTreePlace->activeBanners->count() > 0)
                                        <div id="owl-company-list-3" class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($companyListTreePlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12 bot-panel">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-8">
                                        {{ $companies->links('frontend.includes.pagination', ['paginator' => $companies]) }}
                                    </div>
                                    <div class="col-lg-6 col-sm-4 show-more">
                                        @if ($companies->lastPage() != $companies->currentPage())
                                            <a href="">
                                                <div class="view-more-button">Показать еще</div>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 direct">
                                <img src="/images/ad.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
