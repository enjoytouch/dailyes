@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.event-category')
@endsection

@push('after-scripts')
    <script>
        $('.events-container').infiniteScroll({
            path: '.pagination__next',
            append: '.event-item',
            button: '.view-more-button',
            // using button, disable loading on scroll
            scrollThreshold: false,
            // status: '.page-load-status',
        });
    </script>
@endpush

@section('title', 'Каталог событий' . (empty($category) ? '' : ' - ' . $category->name))
@section('description', 'Каталог событий' . (empty($category) ? '' : ' - ' . $category->name))

@section('content')
    <div class="container-fluid search-panel">
        <div class="container minus-15">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-panel__sort">
                        <ul>
                            @if(!empty($category))
                                <li></li>
                            @else
                                <li class="click-clendar @if(request()->date) active @endif "><img
                                            src="/images/icons/calendar.svg"/><img
                                            src="/images/icons/calendar-blue.svg"/>
                                </li>
                                <li>
                                    <a href="{{ route('frontend.event.index', ['date' => date('Y-m-d') . '_' . date('Y-m-d')]) }}">Сегодня</a>
                                </li>
                                <li><a href="{{ route('frontend.event.index', ['favorite' => 1]) }}">Рекомендуем</a>
                                </li>
                                <li><a href="{{ route('frontend.event.index', ['sort-raiting' => 'desc']) }}">Лучшие
                                        оценки</a></li>
                                <li>
                                    <a href="{{ route('frontend.event.index', ['sort-views' => 'desc']) }}">Популярные</a>
                                <li><a href="{{ route('frontend.event.index') }}">Все события</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="events-block">
                        <div class="row events-container bootfix-row">
                            @foreach($events as $key => $event)
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 event-item bootfix-col">
                                    <div class="events-block__image">
                                        <a href="{{ route('frontend.event.show', $event) }}"><img
                                                    src="{{ $event->image_url }}"/></a>
                                        @if($event->favorite)
                                            <div class="events-block__cool">
                                                <img src="/images/icons/success.svg"/> Выбор редакции
                                            </div>
                                        @endif
                                        <div class="events-block__badges">
                                            @foreach($event->labels as $label)
                                                <div class="events-block__badges-group">
                                                    <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                                    {{ $label->name }}
                                                </div>
                                            @endforeach
                                        </div>
                                        <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                                  :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                                    </div>
                                    <div class="events-block__title">
                                        <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                                    </div>
                                    <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $event->categories[0] : $category]) }}"
                                       class="events-block__group-a">
                                        <span class="events-block__group">{{ empty($category) ? $event->categories[0]->name : $category->name }}</span>
                                    </a>
                                    <div class="events-block__text-desc">
                                        <a href="{{ route('frontend.company.show', $event->company) }}">{{ $event->company->name }}</a>
                                    </div>
                                    <div class="events-block__text-desc">
                                        @if($event->status == 'active')
                                            Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($event->status == 'before')
                                            Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($event->status == 'after')
                                            Завершено
                                        @endif
                                    </div>
                                    <div class="events-block__text-icons v-align">
                                        <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                                        <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                                        <img src="/images/icons/eye.svg"/> {{ $event->views }}
                                    </div>
                                </div>
                                @if(($key + 1) == 8)
                                    @if($eventListOnePlace->activeBanners->count() > 0)
                                        <div id="owl-event-list-1"
                                             class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($eventListOnePlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @elseif(($key + 1) == 16)
                                    @if($eventListSecondPlace->activeBanners->count() > 0)
                                        <div id="owl-event-list-2"
                                             class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($eventListSecondPlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @elseif(($key + 1) == 32)
                                    @if($eventListTreePlace->activeBanners->count() > 0)
                                        <div id="owl-event-list-3"
                                             class="col-md-12 text-center ad bootfix-col owl-carousel owl-theme">
                                            @foreach($eventListTreePlace->activeBanners as $activeBanner)
                                                <a href="{{ $activeBanner->link }}" target="_blank">
                                                    <img src="{{ $activeBanner->download->url }}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12 bootfix-col">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-8">
                                        {{ $events->links('frontend.includes.pagination', ['paginator' => $events]) }}
                                    </div>
                                    <div class="col-lg-6 col-sm-4 show-more">
                                        @if ($events->lastPage() != $events->currentPage())
                                            <a href="">
                                                <div class="view-more-button">Показать еще</div>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-md-12 direct">--}}
                            {{--<img src="/images/ad.png"/>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="map-content">
                <div class="map-content__header">
                    <span>События на карте</span>
                    <div class="map-content__header-close" data-dismiss="modal" aria-label="Close"><img
                                src="/images/icons/close.svg"/></div>
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 100%; height: 600px">
                        <addresses-map :addresses="{{ $addresses }}" :modal="true"></addresses-map>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
