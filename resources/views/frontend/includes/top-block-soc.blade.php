@push('after-scripts')
    <script src="https://yastatic.net/share2/share.js"></script>
@endpush

<div class="top-block__soc">
    <ul>
        <a href="javascript://" class="ya-share2" data-services="vkontakte"><li><img src="/images/icons/vk.svg"></li></a>
        <a href="javascript://" class="ya-share2" data-services="facebook"><li><img src="/images/icons/fb.svg"></li></a>
        <a href="javascript://" class="ya-share2" data-services="odnoklassniki"><li><img src="/images/icons/ok.svg"></li></a>
    </ul>
</div>
