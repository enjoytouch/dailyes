<div class="auth-panel">
    <a class="auth-panel__close authPanel"><img src="/images/icons/close.svg"/></a>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span id="change" class="auth-panel__title">Добро пожаловать!</span>
            </div>
            <div class="col-md-12">
                <div class="auth-panel__option">
                    <div class="auth-panel__option-login active">Войти</div>
                    <div class="auth-panel__option-register">Регистрация</div>
                    <div class="auth-panel__option-forgot">Забыли пароль?</div>
                    <div class="auth-panel__option-full">
                        <div class="auth-panel__option-full-login active-full"></div>
                        <div class="auth-panel__option-full-register"></div>
                        <div class="auth-panel__option-full-forgot"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <login></login>
                <register></register>
                <forgot></forgot>
            </div>
        </div>
    </div>
</div>