<div class="col-md-12 subscribe-block">
    <div class="subscribe-block__text">
        <div>Подпишитесь на новости</div>
        <div>Рассылка не содержит рекламных материалов</div>
    </div>
    <div class="subscribe-block__search">
        <subscriber></subscriber>
    </div>
    <div class="subscribe-block__search-politic"><img src="/images/icons/check_black.svg"/>
        Подписываясь на рассылку, вы соглашаетесь на получение информационных сообщений нашего
        портала в соответствии с <a href="{{ route('page.pravila-rassylok') }}">правилами рассылок</a></div>
</div>
