<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 mobile__nav">
            <div class="container mw">
                <div>
                    <div class="v-align">
                        <div class="mobile__nav-menu">
                            <img src="/images/icons/mobile-menu.svg"/>
                        </div>
                        <div class="mobile__nav-city" @click="$bus.$emit('showCitySelectPanel')">
                            <span class="cityPanel">{{ $userCity->name }}</span>
                        </div>
                        <div class="mobile__nav-login">
                            @guest
                                <span class="authPanel">Войти</span>
                            @endguest
                            @auth
                                <div class="mobile__nav-profile">
                                    <div class="v-align">
                                        <div class="mobile__nav-photo">
                                            <img src="{{Auth::user()->avatar_url}}"/>
                                        </div>
                                        <img class="icon" src="/images/icons/profile-open.svg"/>
                                    </div>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
                @auth
                    <div class="mobile__nav-profileMenu">
                        <ul>
                            @role('admin')
                            <li><a href="{{ route('admin.home') }}">Управление</a></li>
                            @endrole
                            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
                            <li><a href="{{ route('cabinet.notification.index') }}">Уведомления</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                @endauth
                <div class="mobile__nav-fix">
                    <ul>
                        <li>
                            <div class="v-align"><a href="/">Главная</a></div>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="{{ route('frontend.city.event.category.show', [$city, null]) }}">События</a></li>
                        <li><a href="{{ route('frontend.city.company.category.show', [$city, null]) }}">Компании</a>
                        </li>
                        <li><a href="/about">О сервисе</a></li>
                    </ul>
                    <ul>
                        <li><a href="/jobs">Вакансии</a></li>
                        <li><a href="/help">Помощь</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                    </ul>
                    <ul>
                        <li><a href="/for-your-business">Для вашего бизнеса</a></li>
                        <li><a href="/add-company">Добавить компанию</a></li>
                        <li><a href="/placement-rules">Правила размещения</a></li>
                        <li><a href="/advertising-on-the-website">Реклама на сайте</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="@if( !empty(explode('/', url()->current())[3]) && explode('/', url()->current())[3] == 'cabinet') container-fluid @else container @endif top-block">
        <div class="top-block__logo">
            <a href="/"><img src="/images/logo.svg"/></a>
            <a href="/"><img src="/images/logo-mob.svg"/></a>
        </div>
        <div class="top-block__city">
            <div @click="$bus.$emit('showCitySelectPanel')"><span class="current">{{ $userCity->name }}</span>
            </div>
            <city_option></city_option>
        </div>
        <div class="top-block__menu">
            <ul>
                <li><a href="{{ route('frontend.city.event.category.show', [$city, null]) }}">События</a></li>
                <li><a href="{{ route('frontend.city.company.category.show', [$city, null]) }}">Компании</a></li>
                <li><a href="/about">О сервисе</a></li>
            </ul>
        </div>
        @guest
            <div class="top-block__button">
                <a data="1" class="authPanel">Войти</a>
            </div>
        @endguest
        @auth
            <div class="top-block__avatar">
                <div class="v-align">
                    <div class="top-block__avatar-photo">
                        <img src="{{Auth::user()->avatar_url}}"/>
                    </div>
                    <span class="name">{{Auth::user()->name}}</span>
                    <img src="/images/icons/arrow-down-sign-to-navigate.svg"/>
                </div>
                <div class="top-block__open">
                    @role('admin')
                    <div class="top-block__open-item">
                        <a href="{{ route('admin.home') }}">
                            <div class="top-block__open-img"><img src="/images/icons/user.svg"/></div>
                            <span>Управление</span></a>
                    </div>
                    @endrole
                    <div class="top-block__open-item">
                        <a href="{{ route('cabinet.home') }}">
                            <div class="top-block__open-img"><img src="/images/icons/user.svg"/></div>
                            <span>Личный кабинет</span></a>
                    </div>
                    <div class="top-block__open-item">
                        <a href="{{ route('cabinet.notification.index') }}">
                            <div class="top-block__open-img"><img src="/images/icons/notify.svg"/></div>
                            <div class="top-block__open-count">{{ Auth::user()->unreadNotifications()->count() }}</div>
                            <span>Уведомления</span></a>
                    </div>
                    <div class="top-block__open-exit">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <div class="top-block__open-img"><img src="/images/icons/exit.svg"/></div>
                            <span>Выйти</span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        @endauth
    </div>
</div>
