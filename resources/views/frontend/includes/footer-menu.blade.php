<div class="col-md-12">
    <div class="footer-info">
        <hr>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-5">
                <a href=""><img src="/images/logo_footer.svg"/></a>
                <div class="footer-info__copy">© 2019 Портал мероприятий вашего города<br>Все права
                    защищены
                </div>
                <div class="footer-info__agree"><a href="/polzovatelskoe-soglashenie">Пользовательское соглашение</a></div>
                <div class="footer-info__soc">
                    <a href="javascript://" onClick="$('.ya-share2__item_service_vkontakte').click();"><img src="/images/icons/footer_vk.svg"></a>
                    <a href="javascript://" onClick="$('.ya-share2__item_service_facebook').click();"><img src="/images/icons/footer_fb.svg"></a>
                    <a href="javascript://" onClick="$('.ya-share2__item_service_odnoklassniki').click();"><img src="/images/icons/footer_ok.svg"></a>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 hm">
                <div class="footer-info__menu-title">
                    Портал
                </div>
                <ul class="footer-info__menu-ul">
                    <li><a href="/about">О сервисе</a></li>
                    <li><a href="/jobs">Вакансии</a></li>
                    <li><a href="/help">Помощь</a></li>
                    <li><a href="/contacts">Контакты</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-4 hm">
                <div class="footer-info__menu-title">
                    Информация
                </div>
                <ul class="footer-info__menu-ul">
                    <li><a href="/for-your-business">Для вашего бизнеса</a></li>
                    <li><a href="/add-company">Добавить компанию</a></li>
                    <li><a href="/placement-rules">Правила размещения</a></li>
                    <li><a href="/advertising-on-the-website">Реклама на сайте</a></li>
                </ul>
            </div>
            <div class="col-xl-4 col-lg-3 col-md-12 hm">
                <div class="footer-info__soc-general">
                    <div class="footer-info__soc-block">
                        <img src="/images/icons/footer_apple.svg"/>
                        <div class="footer-info__soc-block-text">
                            <div>Доступно в</div>
                            App Store
                        </div>
                    </div>
                    <div class="footer-info__soc-block">
                        <img src="/images/icons/footer_google.svg"/>
                        <div class="footer-info__soc-block-text">
                            <div>Доступно в</div>
                            Google Play
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
