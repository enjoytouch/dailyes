@section('header-id', 'header-min-item')

@if(empty($company))
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('/images/background.png'); background-size: cover; background-position: center center;")
@else
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('$company->background_url'); background-size: cover; background-position: center center;")
@endif

@section('header-class', "resp-header full-height")

<div class="container mw">
    <div class="top-block top-comp-mob">
        <div class="top-block__breadcrumb">
            <ul>
                <li><a href="{{ route('frontend.city.show', $city) }}">Главная</a></li>
                <li><a href="{{ route('frontend.city.company.category.show', $city) }}">Компании</a></li>
                <li>{{ $company->name }}</li>
            </ul>
        </div>
        <div class="top-block__top-badges">
            <div class="v-align">
                <img src="/images/icons/thumb-up-white.svg"/> {{ $company->rec }}% <img
                        src="/images/icons/star-white.svg"/> {{ round($company->star, 1) }}
            </div>
        </div>
        <div class="top-block__title">
            {{ $company->name }}
        </div>
        <div class="top-block__descfull">
            {!! nl2br(e($company->summary)) !!}
        </div>
        @include('frontend.includes.top-block-soc')
        <div class="top-block__ui">
            <div class="v-align">
                <div class="top-block__ui-stars">
                    <span>Поставьте оценку</span>
                    <div class="star-rating">
                        <div class="star-rating__wrap">
                            <company-rating-header :user-rating="{{ json_encode($company->user_rating) }}"></company-rating-header>
                        </div>
                    </div>
                </div>
                <div class="top-block__ui-rec">
                    <span>Поставьте оценку</span>
                    <div class="thumb-rating">
                        <div class="thumb-rating__wrap">
                            <company-recommendation-header :user-recommendation="{{ json_encode($company->user_recommendation) }}"></company-recommendation-header>
                        </div>
                    </div>
                </div>
                <div class="top-block__ui-more">
                    <span>&nbsp;</span>
                    <div class="top-block__ui-group">
                        <company-bookmark :company="{{ json_encode($company->only('slug')) }}" :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_companies->contains('id', $company->id) : false) }}"></company-bookmark>
                        <div class="events-block__badges-sad" title="Пожаловаться"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-block__soc-mob comp">
            @include('frontend.includes.soc-mobile')
        </div>
        <div class="top-block__nav comp-md-nav">
            <div class="v-align">
                <div data-toggle="modal" data-target="#map-modal" class="top-block__nav-map">
                    <img src="/images/icons/placeholder-white.svg"/> <span>Смотреть все адреса компании</span>
                    <div class="count">{{ $company->addresses->count() }}</div>
                </div>
                <div class="top-block__soc-mob comp-right">
                    @include('frontend.includes.soc-mobile')
                </div>
            </div>
        </div>
    </div>
</div>
