@section('header-id', 'header-min-item')

@if(empty($event))
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('/images/background.png'); background-size: cover; background-position: center center;")
@else
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('$event->background_url'); background-size: cover; background-position: center center;")
@endif

@section('header-class', "full-height")

<div class="container mw">
    <div class="top-block">
        <div class="top-block__breadcrumb">
            <ul>
                <li><a href="{{ route('frontend.city.show', $city) }}">Главная</a></li>
                <li><a href="{{ route('frontend.city.event.category.show', $city) }}">События</a></li>
                <li>{{ $event->name }}</li>
            </ul>
        </div>
        <div class="top-block__top-badges">
            <div class="v-align">
                @if($event->favorite)
                    <div class="choice"><img src="/images/icons/choice-red.svg"> Выбор редакции</div>
                @endif
                @foreach($event->labels as $label)
                    <div class="badge_f">
                        <div class="badge_f-image"><img src="{{ $label->icon_url }}"></div>
                        {{ $label->name }}
                    </div>
                @endforeach
                <div class="stat">
                    <img src="/images/icons/thumb-up-white.svg"/> {{ $event->rec }}% <img
                            src="/images/icons/star-white.svg"/> {{ round($event->star, 1) }} <img
                            src="/images/icons/eye-white.svg"/> {{ $event->views }}
                </div>
            </div>
        </div>
        <div class="top-block__title">
            {{ $event->name }}
        </div>
        <div class="top-block__descfull">
            {!! nl2br(e($event->summary)) !!}
        </div>
        @foreach($event->categories as $category)
            <div class="top-block__desc">
                {{ $category->name }}
            </div>
        @endforeach
        @include('frontend.includes.top-block-soc')
        <div class="top-block__ui">
            <div class="v-align">
                <div class="top-block__ui-stat">
                    @if($event->status == 'active')
                        <div>До окончания осталось:
                            <span>{{ $event->end->locale('ru')->diffForHumans(null, true) }}</span>
                        </div>
                    @elseif($event->status == 'before')
                        <div>Дата начала: <span>{{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}</span></div>
                    @elseif($event->status == 'after')
                        <div>Завершено: <span>{{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}</span></div>
                    @endif
                    <div>За сегодня просмотрели: <span>{{ $event->views_today->count ?? 0 }} {{pular($event->views_today->count ?? 0, ['человек', 'человека', 'человек'])}}</span>
                    </div>
                </div>
                <div class="top-block__ui-stars">
                    <span>Поставьте оценку</span>
                    <div class="star-rating">
                        <div class="star-rating__wrap">
                            <rating-header :user-rating="{{ json_encode($event->user_rating) }}"></rating-header>
                        </div>
                    </div>
                </div>
                <div class="top-block__ui-rec">
                    <span>Рекомендовать</span>
                    <div class="thumb-rating">
                        <div class="thumb-rating__wrap">
                            <recommendation-header
                                    :user-recommendation="{{ json_encode($event->user_recommendation) }}"></recommendation-header>
                        </div>
                    </div>
                </div>
                <div class="top-block__ui-more">
                    <span>&nbsp;</span>
                    <div class="top-block__ui-group">
                        <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                  :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                        <div class="events-block__badges-sad" title="Пожаловаться"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-block__soc-mob">
            @include('frontend.includes.soc-mobile')
        </div>
        <div class="top-block__nav">
            <div class="v-align">
                <div class="top-block__nav-date">
                    <img src="/images/icons/calendar-white.svg"/>
                    С {{ $event->start->locale('ru')->isoFormat('D MMMM') }}
                    до {{ $event->end->locale('ru')->isoFormat('D MMMM') }}
                </div>
                <div data-toggle="modal" data-target="#map-modal" class="top-block__nav-map">
                    <img src="/images/icons/placeholder-white.svg"/> <span>Смотреть адреса проведения события</span>
                    <div class="count">{{ $event->addresses->count() }}</div>
                </div>
                <div class="top-block__nav-company">
                    <div class="logo">
                        @empty($event->company->icon)
                            {{ $event->company->name }}
                        @else
                            <img src="{{ $event->company->icon_url }}"/>
                        @endempty
                    </div>
                    <a href="{{ route('frontend.company.show', $event->company) }}">
                        <div>Компания этого события</div>
                        <div><img src="/images/icons/left-arrow.svg"/></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
