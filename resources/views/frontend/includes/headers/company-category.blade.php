@section('header-id', 'min-header-wi')

@if(empty($category))
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('/images/background.png'); background-size: cover; background-position: center center;")
@else
    @section('header-style', "background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('$category->background_url'); background-size: cover; background-position: center center;")
@endif

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.4.2/dist/js/jquery.suggestions.min.js"></script>
    <script>
        if($('#filter_2').length){
            if(!$('#filter_1').length){
                $('#filter_2').click();
            }
        }
        $('#ex21').bootstrapSlider();
        $('#ex21mob').bootstrapSlider();

        var sort_raiting = $('.change-sort-raiting').find('input').val();
        if(sort_raiting <= 2){
            if(sort_raiting == 1){
                $('.change-sort-raiting').find('.iup').show();
                $('.change-sort-raiting').addClass('active');
            }
            if(sort_raiting == 2){
                $('.change-sort-raiting').find('.idown').show();
                $('.change-sort-raiting').find('.iup').hide();
                $('.change-sort-raiting').addClass('active');
            }
        }

        function change_range(num) {
            $("#ex21").bootstrapSlider('setValue', num);
            if(num == 1) {$('#range').val('100')};
            if(num == 2) {$('#range').val('500')};
            if(num == 3) {$('#range').val('1000')};
            if(num == 4) {$('#range').val('5000')};
            if(num == 5) {$('#range').val('10000')};
        }

        $('#ex21').bind('change', function(){
            var i = $(this).val();
            if(i == 1) {$('#range').val('100')};
            if(i == 2) {$('#range').val('500')};
            if(i == 3) {$('#range').val('1000')};
            if(i == 4) {$('#range').val('5000')};
            if(i == 5) {$('#range').val('10000')};
        });

        function change_rangemob(num) {
            $("#ex21mob").bootstrapSlider('setValue', num);
            if(num == 1) {$('#range').val('100')};
            if(num == 2) {$('#range').val('500')};
            if(num == 3) {$('#range').val('1000')};
            if(num == 4) {$('#range').val('5000')};
            if(num == 5) {$('#range').val('10000')};
        }

        $('#ex21mob').bind('change', function(){
            var i = $(this).val();
            if(i == 1) {$('#range').val('100')};
            if(i == 2) {$('#range').val('500')};
            if(i == 3) {$('#range').val('1000')};
            if(i == 4) {$('#range').val('5000')};
            if(i == 5) {$('#range').val('10000')}
            console.log($('#range').val());
        });
    </script>
@endpush

@section('header-class', "mobile-company-height")

<div class="container mw">
    <div class="top-block">
        <div class="top-block__breadcrumb">
            <ul>
                <li><a href="/">Главная</a></li>
                @if(empty($category))
                    <li>
                        Компании
                    </li>
                @else
                    <li>
                        <a href="{{ route('frontend.city.company.category.show', [$city, null]) }}">Компании</a>
                    </li>
                    <li>Компании категории {{ $category->name }}</li>
                @endif
            </ul>
        </div>
        <h1>
            Компании @if(!empty($category))категории {{ $category->name }}@endif
        </h1>
        <div class="top-block__desc">
            Впишите в поисковую строку название компании
        </div>
        <div class="search-block">
            <form id="filter_form">
                <div class="flex-box">
                    <input class="search-block__input filtm comp-search" placeholder="Поиск компаний" value="{{request()->text}}" name="text" type="text"/>
                    <button class="search-block__glass">
                        <img src="/images/icons/glass.svg"/>
                    </button>
                </div>
                <div class="search-block__filter">Фильтр<img class="search-block__filter-img"
                                                             src="/images/icons/filter-open.svg"/></div>
                <div class="search-block__filter-block">
                    <div class="search-block__filter-left">
                        <span class="search-block__filter-left-title">Фильтр по:</span>
                        <ul>
                            @if(empty($category))
                            <li id="filter_2">
                                <img src="/images/icons/filter-category.svg"/> Категориям <img
                                        class="search-block__filter-left-ri" src="/images/icons/filter-left.svg"/>
                            </li>
                            @endif
                            <li id="filter_3">
                                <img src="/images/icons/filter-geo.svg"/> Геолокации <img
                                        class="search-block__filter-left-ri" src="/images/icons/filter-left.svg"/>
                            </li>
                        </ul>
                        <span class="search-block__filter-left-title">Сортировать по:</span>
                        <ul>
                            @php
                                $sortVariants = collect([
                                    0 => '',
                                    1 => 'asc',
                                    2 => 'desc'
                                ]);
                            @endphp
                            <li class="change-sort-raiting">
                                <input type="hidden"
                                       data-sort="{{ $sortVariants->search(request('sort-raiting') ?? 0) }}"
                                       class="sort-raiting" value="{{ request('sort-raiting') }}">
                                <img style="margin-top: -5px;" src="/images/icons/filter-raiting.svg">Рейтингу <i
                                        class="fas fa-long-arrow-alt-up iup"></i> <i
                                        class="fas fa-long-arrow-alt-down idown"></i>
                            </li>
                        </ul>
                    </div>
                    <div class="search-block__filter-right">
                        <div class="filter_2">
                            <div class="left_block">
                                <ul>
                                    @foreach($categories->flatten()->unique('id')->values()->all() as $categoryItem)
                                        <li>
                                            <input id="check_{{$categoryItem->id}}" type="checkbox" name="categories[]"
                                                   value="{{$categoryItem->id}}"
                                                   @if(!empty(request('categories')) && is_array(request('categories')) && in_array($categoryItem->id, request('categories'))) checked @endif/>
                                            <label class="label" for="check_{{$categoryItem->id}}"></label>
                                            <label for="check_{{$categoryItem->id}}">{{$categoryItem->name}}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="filter_3">
                            <div class="filter_3-title">Введите адрес, где вы находитесь</div>
                            <input type="text" name="location[name]" id="address" placeholder="Адрес"
                                   value="{{request('location')['name']}}"/>
                            <div class="rangeBlock">
                                <div class="filter_3-desc">
                                    <label for="adres">Отобразить события рядом с вами и поблизости, но не дальше
                                        чем:</label></div>
                                <div>
                                    <div class="range-width">
                                        @php
                                            $dataSliderTicks = collect([
                                                1 => 100,
                                                2 => 500,
                                                3 => 1000,
                                                4 => 5000,
                                                5 => 10000
                                            ]);
                                        @endphp
                                        <input id="ex21" type="text" data-slider-ticks="{{ $dataSliderTicks->keys() }}"
                                               data-slider-min="1"
                                               data-slider-max="5" data-slider-step="1"
                                               data-slider-value="{{ $dataSliderTicks->search(request('location')['range'] ?? 1000) }}"
                                               data-slider-tooltip="hide"/>
                                        <input type="hidden" id="range" name="location[range]"
                                               value="{{ request('location')['range'] ?? 1000 }}"/>
                                    </div>
                                    <div class="filter_3-range-box">
                                        <div onclick="change_range(1)"><img src="/images/icons/filter_range.svg"/><br>100м
                                        </div>
                                        <div onclick="change_range(2)"><img src="/images/icons/filter_range.svg"/><br>500м
                                        </div>
                                        <div onclick="change_range(3)"><img src="/images/icons/filter_range.svg"/><br>1км
                                        </div>
                                        <div onclick="change_range(4)"><img src="/images/icons/filter_range.svg"/><br>5км
                                        </div>
                                        <div onclick="change_range(5)"><img src="/images/icons/filter_range.svg"/><br>10км
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="search-block__filter-buttons">
                            <div class="clear_filter"><i class="fas fa-times"></i>&nbsp;Сбросить фильтр</div>
                            <button type="submit" class="submit_filter">Найти</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@section('filter-panel')
    <div class="filter-panel">
        <a class="filter-panel__close filterPanel"><img src="/images/icons/close.svg"/></a>
        <div class="filter-panel__block">
            <form id="filter_form-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="filter-panel__block-title">Фильтр компаний</div>
                        </div>
                        <div class="col-md-12 filter-panel__filter">
                            <div class="row">
                                <div class="col-md-4 col-6">
                                    <span>Фильтр по:</span>
                                    <ul>
                                        @if(empty($category))
                                        <li id="filterMob_2">
                                            <div class="v-align"><img src="/images/icons/filter-category.svg">Категориям
                                            </div>
                                        </li>
                                        @endif
                                        <li id="filterMob_3">
                                            <div class="v-align"><img src="/images/icons/filter-geo.svg">Геолокации
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-6">
                                    <span>Сортировать по:</span>
                                    <ul>
                                        <li class="change-sort-raiting">
                                            <input type="hidden"
                                                   data-sort="{{ $sortVariants->search(request('sort-raiting') ?? 0) }}"
                                                   class="sort-raiting" value="{{ request('sort-raiting') }}">
                                            <img src="/images/icons/filter-raiting.svg">Рейтингу <i
                                                    class="fas fa-long-arrow-alt-up iup"></i> <i
                                                    class="fas fa-long-arrow-alt-down idown"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mobfilter_2">
                                <ul>
                                    @foreach($categories as $categoryItem)
                                        <li><input id="check-mob_{{$categoryItem->id}}" type="checkbox"
                                                   name="categories[]"
                                                   value="{{$categoryItem->id}}"
                                                   @if(!empty(request('categories')) && is_array(request('categories')) && in_array($categoryItem->id, request('categories'))) checked @endif>
                                            <label class="label"
                                                   for="check-mob_{{$categoryItem->id}}"></label>
                                            <label for="check-mob_{{$categoryItem->id}}">{{$categoryItem->name}}</label></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="mobfilter_3">
                                <div class="mobfilter_3-title">Введите адрес, где вы находитесь</div>
                                <input id="addressMob" type="text" name="location[name]" placeholder="Адрес" value="{{request('location')['name']}}"/>
                                <div class="rangeBlock">
                                    <div class="mobfilter_3-desc"><label for="adresmob">Отобразить
                                            события рядом с вами и поблизости, но не дальше чем:</label></div>
                                    <div class="range-width">
                                        <input id="ex21mob" type="text" data-slider-ticks="{{ $dataSliderTicks->keys() }}"
                                               data-slider-min="1" data-slider-max="5" data-slider-step="1"
                                               data-slider-value="{{ $dataSliderTicks->search(request('location')['range'] ?? 1000) }}" data-slider-tooltip="hide"/>
                                        <input type="hidden" id="rangeMob" name="location[range]"
                                               value="{{ request('location')['range'] ?? 1000 }}"/>
                                    </div>
                                    <div class="mobfilter_3-range-box">
                                        <div onclick="change_rangemob(1)"><img
                                                    src="/images/icons/filter_range.svg"/><br>100м
                                        </div>
                                        <div onclick="change_rangemob(2)"><img
                                                    src="/images/icons/filter_range.svg"/><br>500м
                                        </div>
                                        <div onclick="change_rangemob(3)"><img
                                                    src="/images/icons/filter_range.svg"/><br>1км
                                        </div>
                                        <div onclick="change_rangemob(4)"><img
                                                    src="/images/icons/filter_range.svg"/><br>5км
                                        </div>
                                        <div onclick="change_rangemob(5)"><img
                                                    src="/images/icons/filter_range.svg"/><br>10км
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row filter-panel__nav">
                                <div class="col-lg-6 col-md-6 col-sm-6 text-left">
                                    <div class="v-align"><span class="clear_filterm"><i class="fas fa-times"></i>&nbsp;Сбросить фильтр</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                                    <button type="submit">Найти</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
