<div class="app-block">
    <div class="container mw">
        <div class="app-block__info">
            <div class="app-block__image">
                <img src="/images/icons/app-download-icon.svg"/>
            </div>
            <div class="app-block__info-title">
                Скачайте мобильное приложение
            </div>
            <div class="app-block__info-desc">
                Еще больше пользы<br>
                для вашего времяпровождения
            </div>
            <div class="app-block__buttons-block">
                {{--<a href="">--}}
                    <div class="app-block__button-back">
                        <div class="app-block__button-logo">
                            <img src="/images/icons/iphone.svg"/>
                        </div>
                        <div class="app-block__button-text">
                            <div>Бесплатно</div>
                            <div>App Store</div>
                        </div>
                    </div>
                {{--</a>--}}
                {{--<a href="">--}}
                    <div class="app-block__button-back">
                        <div class="app-block__button-logo">
                            <img src="/images/icons/android.svg"/>
                        </div>
                        <div class="app-block__button-text">
                            <div>Бесплатно</div>
                            <div>Google Play</div>
                        </div>
                    </div>
                {{--</a>--}}
            </div>
        </div>
        <img class="app-block__phone" src="/images/app-phone.png"/>
    </div>
</div>
