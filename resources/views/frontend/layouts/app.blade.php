<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('frontend.includes.meta')
</head>
<body>
<div id="app" class="oh">
    <header id="@yield('header-id')" style="@yield('header-style')" class="@yield('header-class')">
        @include('frontend.includes.header-menu')
        @yield('header')
    </header>
    @yield('content')
    <div class="to_up">
        <img src="/images/icons/up.svg"/>
    </div>
    <footer>
        <div class="container-fluid">
            <div class="row">
                @include('frontend.includes.app-block')
            </div>
            <div class="container minus-15">
                <div class="row">
                    @include('frontend.includes.subscribe')
                    @include('frontend.includes.footer-menu')
                </div>
            </div>
        </div>
        @guest
            @include('frontend.includes.auth-panel')
        @endguest
        <city_select></city_select>
        @yield('filter-panel')
    </footer>
</div>

<!-- Scripts -->
@stack('before-scripts')
<script src="{{ mix('js/frontend.js') }}"></script>
@stack('after-scripts')

<script src="https://yastatic.net/share2/share.js" async="async"></script>

</body>
</html>
