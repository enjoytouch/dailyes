@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.event')
@endsection

@push('after-scripts')
    <script src="/js/sticky.min.js"></script>
    <script>
        var sticky = new Sticky('.selector');
    </script>
    <script>
        $('#report6').click(function () {
            if ($(this).is(':checked')) {
                $('.report-textarea').show();
            } else {
                $('.report-textarea').hide();
            }
        });
    </script>
@endpush

@section('title', $event->name ?? '')
@section('description', $event->summary ?? '')

@section('content')
    <div class="container-fluid minus-15">
        <div class="row">
            @if($event->status == 'active' && auth()->user() && $event->user && auth()->id() == $event->user->id)
                @if($event->active)
                    <div class="col-md-12 warning-block mh">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <div class="warning-block__text">
                                        <div class="v-align"><img src="/images/icons/event-on.svg"/> Событие в работе
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 warning-block__right">
                                    <div class="v-align">
                                        <div class="warning-block__nav">
                                            <a href="{{ route('cabinet.event.stat', $event) }}">
                                                <div class="warning-block__nav-stat"></div>
                                            </a>
                                            <a href="{{ route('cabinet.event.edit', $event) }}">
                                                <div class="warning-block__nav-pencil"></div>
                                            </a>
                                        </div>
                                        <a href="{{ route('cabinet.event.edit', $event) }}" class="warning-block__stop">Приостановить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12 warning-block mh">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <div class="warning-block__text">
                                        <div class="v-align"><img src="/images/icons/shut-down.svg"/> Событие
                                            остановлено!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 warning-block__right">
                                    <div class="v-align">
                                        <div class="warning-block__nav">
                                            <a href="{{ route('cabinet.event.stat', $event) }}">
                                                <div class="warning-block__nav-stat"></div>
                                            </a>
                                            <a href="{{ route('cabinet.event.edit', $event) }}">
                                                <div class="warning-block__nav-pencil"></div>
                                            </a>
                                        </div>
                                        <a href="{{ route('cabinet.event.edit', $event) }}"
                                           class="warning-block__public">Возобновить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @elseif($event->status == 'after')
                @if(auth()->user() && auth()->id() == $event->user->id)
                    <div class="col-md-12 warning-block mh">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <div class="warning-block__text">
                                        <div class="v-align"><img src="/images/icons/shut-down.svg"/> Внимание, это
                                            событие
                                            завершено!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 warning-block__right">
                                    <div class="v-align">
                                        <div class="warning-block__nav">
                                            <a href="{{ route('cabinet.event.stat', $event) }}">
                                                <div class="warning-block__nav-stat"></div>
                                            </a>
                                            <a href="{{ route('cabinet.event.edit', $event) }}">
                                                <div class="warning-block__nav-pencil"></div>
                                            </a>
                                        </div>
                                        <a href="{{ route('cabinet.event.edit', $event) }}"
                                           class="warning-block__public">Опубликовать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12 text-center warning-block">
                        <div class="warning-block__text">
                            <div class="h-align"><img src="/images/icons/shut-down.svg"/> Внимание, это событие
                                завершено!
                            </div>
                        </div>
                    </div>
                @endif
            @endif

        </div>
    </div>
    <div class="container mw text-global-block">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="text-block">
                    <div class="text-block__title">
                        Описание
                    </div>
                    <div class="text-block__text">
                        {!! nl2br(e($event->about)) !!}
                    </div>
                    <div class="text-block__more">
                        <span>Читать полностью</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 text-center">
                <div class="text-block__ad-fixed mobhb">
                    @if($eventOnePlace->activeBanners->count() > 0)
                        <div id="owl-card-1" class="owl-carousel owl-theme"
                             style="max-width:240px; max-height: 400px; margin: 0px 0px 10px auto;">
                            @foreach($eventOnePlace->activeBanners as $activeBanner)
                                <a href="{{ $activeBanner->link }}" target="_blank">
                                    <img src="{{ $activeBanner->download->url }}" style="display: block; width: 100%;"/>
                                </a>
                            @endforeach
                        </div>
                    @else
                        <img src="/images/black-friday.png"/>
                    @endif
                    <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                </div>
                <div class="scroll_ad adhd">
                    <ul>
                        <li>
                            <div class="text-block__ad-fixed">
                                @if($eventOnePlace->activeBanners->count() > 0)
                                    <div id="owl-card-2" class="owl-carousel owl-theme"
                                         style="max-width:240px; max-height: 400px; margin: 0 auto; margin-bottom: 10px;">
                                        @foreach($eventOnePlace->activeBanners as $activeBanner)
                                            <a href="{{ $activeBanner->link }}" target="_blank">
                                                <img src="{{ $activeBanner->download->url }}"/>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <img src="/images/black-friday.png"/>
                                @endif
                                <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="text-block__ad-fixed">
                                @if($eventSecondPlace->activeBanners->count() > 0)
                                    <div id="owl-card-3" class="owl-carousel owl-theme"
                                         style="max-width:240px; max-height: 400px; margin: 0 auto; margin-bottom: 10px;">
                                        @foreach($eventSecondPlace->activeBanners as $activeBanner)
                                            <a href="{{ $activeBanner->link }}" target="_blank">
                                                <img src="{{ $activeBanner->download->url }}"/>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <img src="/images/black-friday.png"/>
                                @endif
                                <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                @if($event->gallery_items->count() > 0)
                    <div class="text-block__title">
                        Медиагалерея
                    </div>
                    <div class="mediagalleryMob">
                        <div id="owl-gallery" class="owl-carousel">
                            @foreach($event->gallery_items as $gallery_item)
                                @if($gallery_item->attachable_type == 'App\Video' || ($loop->iteration == 6 && $loop->remaining > 0))
                                    <div class="item">
                                        <div class="block-gallery">
                                            <img class="block-gallery__photo"
                                                 src="{{ $gallery_item->attachable->url }}"/>
                                            <a data-fancybox="gallery-mob"
                                               href="{{ $gallery_item->attachable_type == 'App\Video' ? $gallery_item->attachable->link : $gallery_item->attachable->url }}">
                                                <div class="block-gallery__shadow">
                                                    <div class="h-align">
                                                        <div class="v-align">
                                                            @if($loop->iteration == 6 && $loop->remaining > 0)
                                                                <div class="block-gallery__shadow-title">
                                                                    +{{ $loop->remaining }}</div>
                                                            @elseif($gallery_item->attachable_type == 'App\Video')
                                                                <img src="/images/icons/play-arrow.svg"/>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="item">
                                        <div class="block-gallery">
                                            <a data-fancybox="gallery-mob"
                                               href="{{ $gallery_item->attachable->url }}">
                                                <img class="block-gallery__photo"
                                                     src="{{ $gallery_item->attachable->url }}"/>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if($loop->iteration == 6)
                                    @break
                                @endif
                            @endforeach
                        </div>
                        <div hidden>
                            @foreach($event->gallery_items as $gallery_item)
                                @if($loop->iteration <= 6)
                                    @continue
                                @else
                                    @if($gallery_item->attachable_type == 'App\Video')
                                        <a data-fancybox="gallery-mob" href="{{ $gallery_item->attachable->link }}"></a>
                                    @elseif($gallery_item->attachable_type == 'App\Download')
                                        <a data-fancybox="gallery-mob"
                                           href="{{ $gallery_item->attachable->url }}"></a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="row block-gallery__global">
                        @foreach($event->gallery_items as $gallery_item)
                            @if($gallery_item->attachable_type == 'App\Video' || ($loop->iteration == 6 && $loop->remaining > 0))
                                <div class="col-md-4">
                                    <div class="block-gallery">
                                        <img class="block-gallery__photo" src="{{ $gallery_item->attachable->url }}"/>
                                        <a data-fancybox="gallery"
                                           href="{{ $gallery_item->attachable_type == 'App\Video' ? $gallery_item->attachable->link : $gallery_item->attachable->url }}">
                                            <div class="block-gallery__shadow">
                                                <div class="h-align">
                                                    <div class="v-align">
                                                        @if($loop->iteration == 6 && $loop->remaining > 0)
                                                            <div class="block-gallery__shadow-title">
                                                                +{{ $loop->remaining }}</div>
                                                        @elseif($gallery_item->attachable_type == 'App\Video')
                                                            <img src="/images/icons/play-arrow.svg"/>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-4">
                                    <div class="block-gallery">
                                        <a data-fancybox="gallery" href="{{ $gallery_item->attachable->url }}">
                                            <img class="block-gallery__photo"
                                                 src="{{ $gallery_item->attachable->url }}"/>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if($loop->iteration == 6)
                                @break
                            @endif
                        @endforeach
                        <div hidden>
                            @foreach($event->gallery_items as $gallery_item)
                                @if($loop->iteration <= 6)
                                    @continue
                                @else
                                    @if($gallery_item->attachable_type == 'App\Video')
                                        <a data-fancybox="gallery" href="{{ $gallery_item->attachable->link }}"></a>
                                    @elseif($gallery_item->attachable_type == 'App\Download')
                                        <a data-fancybox="gallery"
                                           href="{{ $gallery_item->attachable->url }}"></a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="text-block__title">
                    Событие на карте
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <addresses-carousel :addresses="{{ $event->addresses }}"></addresses-carousel>
                    </div>
                    <div class="col-md-12 paddMap">
                        <div id="mapList">
                            <addresses-map :addresses="{{ $event->addresses }}" :carousel="true"></addresses-map>
                        </div>
                    </div>
                </div>
                <div class="text-block__title">
                    Отзывы и вопросы
                </div>
                <div class="reviews-nav">
                    <div class="reviews-nav__reviews active">
                        <span>Отзывы</span>
                        <div class="count">{{ $event->comments ? $event->comments->count() : 0 }}</div>
                    </div>
                    <div class="reviews-nav__questions">
                        <span>Вопросы</span>
                        <div class="count">{{ $event->questions ? $event->questions->count() : 0 }}</div>
                    </div>
                </div>
                <div class="reviews-rating">
                    <div class="reviews-rating__count">
                        <span>{{ round($event->star, 1) }}</span> / Из 5
                    </div>
                    <div class="reviews-rating__stars">
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-blue"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-blue"></label><label
                                class="star-raiting-white"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><br>
                        <label class="star-raiting-blue"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label><label class="star-raiting-white"></label><label
                                class="star-raiting-white"></label>
                    </div>
                    <div class="reviews-rating__progress">
                        @for ($i = 5; $i > 0; $i--)
                            <div class="v-align">
                                <div class="reviews-rating__progress-place">
                                    <div style="width: {{ isset($event->rating_results_array[$i]) ? round($event->rating_results_array[$i] / $event->count_stars * 100, 0) : 0 }}%"
                                         class="reviews-rating__progress-bar"></div>
                                </div>
                                <div class="reviews-rating__progress-count">{{ isset($event->rating_results_array[$i]) ? $event->rating_results_array[$i] : 0 }}</div>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="calc-block">
                    <div>Для расчета рейтинга используются общие оценки по характеристикам за все время</div>
                    <div>@if(!auth()->id())Войдите в свой профиль, что бы оставить отзыв, комментарий или задать
                        вопрос@endif</div>
                </div>
            </div>
            <div data-sticky-container class="col-md-3 text-center hide_block">
                <div class="text-block__ad selector" data-margin-top="20">
                    <div>
                        @if($eventSecondPlace->activeBanners->count() > 0)
                            <div id="owl-card-4" class="owl-carousel owl-theme"
                                 style="max-width:240px; max-height: 400px; margin: 0px 0px 10px auto;">
                                @foreach($eventSecondPlace->activeBanners as $activeBanner)
                                    <a href="{{ $activeBanner->link }}" target="_blank">
                                        <img src="{{ $activeBanner->download->url }}"/>
                                    </a>
                                @endforeach
                            </div>
                        @else
                            <img src="/images/black-friday.png"/>
                        @endif
                        <div class="h-align"><a href="/advertising-on-the-website">+ Купить рекламу</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="questions-block">
                    @if(auth()->id())
                        <button class="write_questions">Задать вопрос</button>
                        <div class="onwrite-quest">
                            <div class="v-align">
                                <div class="onwrite-quest__avatar">
                                    <img src="{{ auth()->user()->avatar_url }}"/>
                                </div>
                                <div class="onwrite-quest__title">
                                    {{ auth()->user()->name }}<br>
                                    <span>Пользователь</span>
                                </div>
                            </div>
                            <form action="{{ route('frontend.event.question', $event) }}" method="post">
                                {{ csrf_field() }}
                                <textarea name="review" placeholder="Напишите здесь вопрос"></textarea>
                                <div class="v-align">
                                    <div class="onwrite-quest__button">
                                        <button type="submit">Опубликовать</button>
                                        <span class="closeQuest">Отменить</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12 question-close">
                            @foreach($event->questions as $question)
                                <div class="question">
                                    <div class="question-block">
                                        <div class="v-align">
                                            <div class="question-block__avatar">
                                                <img src="{{ $question->user->avatar_url }}"/>
                                            </div>
                                            <div class="question-block__title">
                                                {{ $question->user->name }}<br>
                                                <span>Дата вопроса: {{ $question->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}</span>
                                            </div>
                                        </div>
                                        <div class="question-block__text">
                                            {{ $question->text }}
                                        </div>
                                        <div class="question-block__nav">
                                            <div class="v-align"><span
                                                        class="question-block__nav-more">Отзыв полностью</span>
                                                <div class="question-block__nav-comments openQuestion">{{ $question->answers->count() }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($question->answers->count() > 0)
                                        <div class="question-block__comments">
                                            <div class="v-align">
                                                <span class="question-block__comments-count">{{ $question->answers->count() }} {{ pular($question->answers->count(), ['комментарий', 'комментария', 'комментариев']) }}</span>
                                                <span class="question-block__comments-hide">Свернуть комментарии <i
                                                            class="fas fa-chevron-up"></i></span>
                                            </div>
                                            <div class="question-block__comments-row">
                                                @foreach($question->answers as $answer)
                                                    <div class="question-block__comments-list">
                                                        <div class="v-align">
                                                            <div class="question-block__comments-list-avatar">
                                                                <img src="{{ $answer->user->avatar_url }}"/>
                                                            </div>
                                                            <div class="question-block__comments-list-title">
                                                                @if($answer->user->hasRole('admin'))
                                                                    Администратор@else{{ $answer->user->name }}@endif
                                                                <br>
                                                                <span>Дата: {{ $answer->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}@if($answer->user->hasRole('admin'))
                                                                        (администратор) @endif</span>
                                                            </div>
                                                        </div>
                                                        <div class="question-block__comments-list-text">
                                                            {{ $answer->text }}
                                                        </div>
                                                        {{--@if(auth()->id() && auth()->id() != $answer->user->id)--}}
                                                        {{--<div class="review-block__comments-list-reply">--}}
                                                        {{--<span>Ответить</span>--}}
                                                        {{--</div>--}}
                                                        {{--@endif--}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="more_review">Показать еще отзывы</button>
                    </div>
                </div>
            </div>
            @if(auth()->id() && !$event->comments->where('text', '!=', null)->contains('user_id', auth()->id()))
                <div class="col-md-12">
                    <button class="write_review">Написать отзыв</button>
                    <div class="onwrite-review">
                        <div class="v-align">
                            <div class="onwrite-review__avatar">
                                <img src="{{ auth()->user()->avatar_url }}"/>
                            </div>
                            <div class="onwrite-review__title">
                                {{ auth()->user()->name }}<br>
                                <span>Пользователь</span>
                            </div>
                        </div>
                        <form action="{{ route('frontend.event.review', $event) }}" method="post">
                            {{ csrf_field() }}
                            <div class="v-align">
                                <div class="onwrite-review__nav">
                                    @if($event->user_rating)
                                        <input type="hidden" name="rating" value="{{ $event->user_rating->star }}">
                                    @else
                                        <span>Поставьте оценку</span>
                                        <div class="onwrite-review__rating">
                                            <div class="onwrite-review__rating-wrap">
                                                <input class="onwrite-review__rating-input" id="star-review-5"
                                                       type="radio"
                                                       name="rating" value="5">
                                                <label class="star-review__ico" for="star-review-5"
                                                       title="5 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-4"
                                                       type="radio"
                                                       name="rating" value="4">
                                                <label class="star-review__ico" for="star-review-4"
                                                       title="4 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-3"
                                                       type="radio"
                                                       name="rating" value="3">
                                                <label class="star-review__ico" for="star-review-3"
                                                       title="3 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-2"
                                                       type="radio"
                                                       name="rating" value="2">
                                                <label class="star-review__ico" for="star-review-2"
                                                       title="2 из 5 звёзд"></label>
                                                <input class="onwrite-review__rating-input" id="star-review-1"
                                                       type="radio"
                                                       name="rating" value="1">
                                                <label class="star-review__ico" for="star-review-1"
                                                       title="1 из 5 звёзд"></label>
                                            </div>
                                        </div>
                                    @endif
                                    @if($event->user_recommendation)
                                        <input type="hidden" name="thumb"
                                               value="{{ $event->user_recommendation->bool }}">
                                    @else
                                        <span>Рекомендуете?</span>
                                        <div class="onwrite-review__blockmob">
                                            <input class="review__thumb-input up" id="thumb-review-2" type="radio"
                                                   name="thumb"
                                                   value="1">
                                            <label class="thumb-review__ico up" for="thumb-review-2"
                                                   title="Нравится"></label>
                                            <input class="review__thumb-input down" id="thumb-review-1" type="radio"
                                                   name="thumb" value="0">
                                            <label class="thumb-review__ico down" for="thumb-review-1"
                                                   title="Не нравится"></label>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <textarea name="review" placeholder="Напишите здесь отзыв"></textarea>
                            <div class="v-align">
                                <div class="onwrite-review__button">
                                    <button type="submit">Опубликовать</button>
                                    <span class="closeReview">Отменить</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <div class="col-md-12 review-close">
                @foreach($event->comments as $comment)
                    <div class="review">
                        <div class="review-block">
                            <div class="review-block__fixed">
                                <div class="v-align">
                                    @if($comment->rec)
                                        <div class="review-block__fixed-thumb-up"></div><span>Рекомендую</span>@endif
                                    <div class="review-block__fixed-star"></div>
                                    <span>{{ $comment->star }}</span>
                                </div>
                            </div>
                            <div class="v-align">
                                <div class="review-block__avatar">
                                    <img src="{{ $comment->user->avatar_url }}"/>
                                </div>
                                <div class="review-block__title">
                                    {{ $comment->user->name }}<br>
                                    <span>Дата отзыва: {{ $comment->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}</span>
                                </div>
                            </div>
                            <div class="review-block__text">
                                {{ $comment->text }}
                            </div>
                            <div class="review-block__nav">
                                <div class="v-align"><span class="review-block__nav-more">Отзыв полностью</span>
                                    <div class="review-block__nav-comments openComments">{{ $comment->answers->count() }}</div>
                                </div>
                            </div>
                        </div>
                        @if($comment->answers->count() > 0)
                            <div class="review-block__comments">
                                <div class="v-align">
                                    <span class="review-block__comments-count">{{ $comment->answers->count() }} {{ pular($comment->answers->count(), ['комментарий', 'комментария', 'комментариев']) }}</span>
                                    <span class="review-block__comments-hide">Свернуть комментарии <i
                                                class="fas fa-chevron-up"></i></span>
                                </div>
                                <div class="review-block__comments-row">
                                    @foreach($comment->answers as $answer)
                                        <div class="review-block__comments-list">
                                            <div class="v-align">
                                                <div class="review-block__comments-list-avatar">
                                                    <img src="{{ $answer->user->avatar_url }}"/>
                                                </div>
                                                <div class="review-block__comments-list-title">
                                                    @if($answer->user->hasRole('admin'))
                                                        Администратор@else{{ $answer->user->name }}@endif<br>
                                                    <span>Дата: {{ $answer->created_at->locale('ru')->isoFormat('D MMMM YYYY') }}@if($answer->user->hasRole('admin'))
                                                            (администратор) @endif</span>
                                                </div>
                                            </div>
                                            <div class="review-block__comments-list-text">
                                                {{ $answer->text }}
                                            </div>
                                            {{--@if(auth()->id() && auth()->id() != $answer->user->id)--}}
                                            {{--<div class="review-block__comments-list-reply">--}}
                                            {{--<span>Ответить</span>--}}
                                            {{--</div>--}}
                                            {{--@endif--}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="col-md-12">
                <button class="more_review">Показать еще отзывы</button>
            </div>
            <div class="col-md-12">
                <div class="attract-block">
                    <div class="v-align">
                        <div class="attract-block__text">
                            Узнайте как привлечь новых клиентов<br>
                            <span>с помощью нашего сервиса</span>
                            <br>
                            <a href="/for-your-business" class="attract-block__button hid">
                                Подробнее
                            </a>
                        </div>
                        <a href="/for-your-business" class="attract-block__button">
                            Подробнее
                        </a>
                    </div>
                    <div class="attract-block__image">
                        <img src="/images/attract.png"/>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="similar-title">
                    Похожие события
                </div>
                <div class="owl-carousel events-block gallery_owl">
                    @foreach($event->related_events as $related_event)
                        <div>
                            <div class="events-block__image">
                                <a href="{{ route('frontend.event.show', $related_event) }}"><img
                                            src="{{ $related_event->image_url }}"/></a>
                                @if($related_event->favorite)
                                    <div class="events-block__cool">
                                        <img src="/images/icons/success.svg"/> Выбор редакции
                                    </div>
                                @endif
                                <div class="events-block__badges">
                                    @foreach($related_event->labels as $label)
                                        <div class="events-block__badges-group">
                                            <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                            {{ $label->name }}
                                        </div>
                                    @endforeach
                                </div>
                                <bookmark :event="{{ json_encode($related_event->only('slug')) }}"
                                          :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $related_event->id) : false) }}"></bookmark>
                            </div>
                            <div class="events-block__title">
                                <a href="{{ route('frontend.event.show', $related_event) }}">{{ $related_event->name }}</a>
                            </div>
                            <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $related_event->categories[0] : $category]) }}"
                               class="events-block__group-a">
                                <span class="events-block__group">{{ empty($category) ? $related_event->categories[0]->name : $category->name }}</span>
                            </a>
                            <div class="events-block__text-desc">
                                <a href="{{ route('frontend.company.show', $related_event->company) }}">{{ $related_event->company->name }}</a>
                            </div>
                            <div class="events-block__text-desc">
                                @if($related_event->status == 'active')
                                    Дата окончания: {{ $related_event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                @elseif($related_event->status == 'before')
                                    Дата начала: {{ $related_event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                @elseif($related_event->status == 'after')
                                    Завершено
                                @endif
                            </div>
                            <div class="events-block__text-icons v-align">
                                <img src="/images/icons/thumb-up-blue.svg"/> {{ $related_event->rec }}%
                                <img src="/images/icons/star-blue.svg"/> {{ round($related_event->star, 1) }}
                                <img src="/images/icons/eye.svg"/> {{ $related_event->views }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <div class="blue_hr"></div>
            </div>
            <div class="col-md-12">
                <div class="similar-title">
                    Недавно просмотренные
                </div>
                <div class="owl-carousel events-block gallery_owl">
                    @foreach(recently_events() as $recently_event)
                        <div>
                            <div class="events-block__image">
                                <a href="{{ route('frontend.event.show', $recently_event) }}"><img
                                            src="{{ $recently_event->image_url }}"/></a>
                                @if($recently_event->favorite)
                                    <div class="events-block__cool">
                                        <img src="/images/icons/success.svg"/> Выбор редакции
                                    </div>
                                @endif
                                <div class="events-block__badges">
                                    @foreach($recently_event->labels as $label)
                                        <div class="events-block__badges-group">
                                            <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                            {{ $label->name }}
                                        </div>
                                    @endforeach
                                </div>
                                <bookmark :event="{{ json_encode($recently_event->only('slug')) }}"
                                          :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $recently_event->id) : false) }}"></bookmark>
                            </div>
                            <div class="events-block__title">
                                <a href="{{ route('frontend.event.show', $recently_event) }}">{{ $recently_event->name }}</a>
                            </div>
                            <a href="{{ route('frontend.city.event.category.show', [$city, empty($category) ? $recently_event->categories[0] : $category]) }}"
                               class="events-block__group-a">
                                <span class="events-block__group">{{ empty($category) ? $recently_event->categories[0]->name : $category->name }}</span>
                            </a>
                            <div class="events-block__text-desc">
                                <a href="{{ route('frontend.company.show', $recently_event->company) }}">{{ $recently_event->company->name }}</a>
                            </div>
                            <div class="events-block__text-desc">
                                @if($event->status == 'active')
                                    Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                @elseif($event->status == 'before')
                                    Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                @elseif($event->status == 'after')
                                    Завершено
                                @endif
                            </div>
                            <div class="events-block__text-icons v-align">
                                <img src="/images/icons/thumb-up-blue.svg"/> {{ $recently_event->rec }}%
                                <img src="/images/icons/star-blue.svg"/> {{ round($recently_event->star, 1) }}
                                <img src="/images/icons/eye.svg"/> {{ $recently_event->views }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="report-panel">
        <a class="auth-panel__close reportPanel"><img src="/images/icons/close.svg"/></a>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="report-panel__title">Почему вы жалуетесь на это событие?</span>
                </div>
                <form id="respform" action="{{ route('frontend.event.report', $event) }}" method="post"
                      style="margin-top: 70px"
                      class="col-md-12">
                    {{ csrf_field() }}
                    <div>
                        <input name="checkbox[0]" value="В описании указана не верная информация" type="checkbox"
                               id="report1" class="report-checkbox"/>
                        <label for="report1" class="label-report"></label>
                        <label for="report1" class="report-checkbox-text">В описании указана не верная
                            информация</label>
                    </div>
                    <div>
                        <input name="checkbox[1]" value="Фотографии не соответствуют действительности" type="checkbox"
                               id="report2" class="report-checkbox"/>
                        <label for="report2" class="label-report"></label>
                        <label for="report2" class="report-checkbox-text">Фотографии не соответствуют
                            действительности</label>
                    </div>
                    <div>
                        <input name="checkbox[2]" value="Это мошенничество" type="checkbox" id="report3"
                               class="report-checkbox"/>
                        <label for="report3" class="label-report"></label>
                        <label for="report3" class="report-checkbox-text">Это мошенничество</label>
                    </div>
                    <div>
                        <input name="checkbox[3]" value="Это оскорбительно" type="checkbox" id="report4"
                               class="report-checkbox"/>
                        <label for="report4" class="label-report"></label>
                        <label for="report4" class="report-checkbox-text">Это оскорбительно</label>
                    </div>
                    <div>
                        <input name="checkbox[4]" value="Событие не существует" type="checkbox" id="report5"
                               class="report-checkbox"/>
                        <label for="report5" class="label-report"></label>
                        <label for="report5" class="report-checkbox-text">Событие не существует</label>
                    </div>
                    <div>
                        <input type="checkbox" id="report6" class="report-checkbox"/>
                        <label for="report6" class="label-report"></label>
                        <label for="report6" class="report-checkbox-text">Проблема в другом</label>
                    </div>
                    <div class="report-textarea">
                        <textarea name="text" class="report-area"></textarea><br>
                    </div>
                    <div>
                        <button type="submit" class="report-submit">Отправить</button>
                    </div>
                </form>
                <h3 class="message-resp">Спасибо, что поделились данной информацией!<br>В ближайшее время мы проверим это событие.<br>
                    <button onclick="$('.reportPanel').click();" class="response-button">Хорошо</button>
                </h3>
            </div>
        </div>
    </div>

    <div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="map-content">
                <div class="map-content__header">
                    <span>События на карте</span>
                    <div class="map-content__header-close" data-dismiss="modal" aria-label="Close"><img
                                src="/images/icons/close.svg"/></div>
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 100%; height: 600px">
                        <addresses-map :addresses="{{ $event->addresses }}" :modal="true"></addresses-map>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
