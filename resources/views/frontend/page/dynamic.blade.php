@extends('frontend.layouts.app')

@section('header')
    @include('frontend.includes.headers.text-page')
@endsection

@section('content')
    <div class="container-fluid search-panel oh text-page">
        <div class="container minus-15">
            <div class="row">
                <div class="col-md-12">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>
@endsection
