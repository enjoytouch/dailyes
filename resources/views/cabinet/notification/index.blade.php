@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Уведомления</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Уведомления
    </div>
    <div class="lk-body__table" style="overflow: auto">
        <table class="text">
            <tbody>
            <tr>
                <td>Дата</td>
                <td>Уведомление</td>
            </tr>
            @foreach($notifications as $notify)
                <tr>
                    <td>{{ $notify->created_at }}</td>
                    <td>{!! $notify->data['text'] !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
