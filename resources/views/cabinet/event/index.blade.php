@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>События</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Мои события
    </div>
    <div class="lk-body__trigger">
        <div class="lk-body__trigger-url-nohref @if(!(boolean) request()->query('completed')) active @endif "><a href="{{ route('cabinet.event.index') }}">Активные</a></div>
        <div class="lk-body__trigger-url-nohref @if((boolean) request()->query('completed')) active @endif "><a href="{{ route('cabinet.event.index', ['completed' => true]) }}">Завершенные</a></div>
    </div>
    @if($events->count() == 0 && !(boolean) request()->query('completed'))
        <div class="lk-body__text">
            У вас не добавлено событий.<br>
            Что бы люди узнали о вашем мероприятии вы можете добавить его на наш портал.
        </div>
        <div class="lk-body__nav">
            <a href="{{ route('cabinet.event.create') }}">
                <div class="lk-body__nav-button v-align"><img src="/images/icons/round-add-button.svg"/>Добавить
                    событие
                </div>
            </a>
        </div>
    @else
        <div>
            <br>
            <a href="{{ route('cabinet.event.create') }}">
                <div class="lk-body__nav-button v-align"><img src="/images/icons/round-add-button.svg"/>Добавить событие
                </div>
            </a>
        </div>
        <div class="lk-body__row">
            <div class="container-fluid">
                <div class="trigger_events">
                    <div class="row">
                        @foreach($events as $event)
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="lk-body__row-img">
                                    <a href="{{ route('cabinet.event.edit', $event) }}">
                                        <div class="events-block__empty"></div>
                                    </a>
                                    <img src="{{ $event->image_url }}"/>
                                    @if($event->favorite)
                                        <div class="events-block__cool">
                                            <img src="/images/icons/success.svg"/> Выбор редакции
                                        </div>
                                    @endif
                                    <div class="events-block__badges">
                                        @foreach($event->labels as $label)
                                            <div class="events-block__badges-group">
                                                <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                                {{ $label->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                    <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                              :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                                </div>
                                <div class="lk-body__row-title">
                                    <a href="{{ route('cabinet.event.edit', $event) }}">{{ $event->name }}</a>
                                </div>
                                <span class="lk-body__row-badge">
                                        {{ $event->categories->count() > 0 ? $event->categories[0]->name : '' }}
                                    </span>
                                <div class="lk-body__row-text">
                                    <span>{{ $event->company->name }}</span>
                                    <span>
                                    @if($event->status == 'active')
                                            Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($event->status == 'before')
                                            Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                        @elseif($event->status == 'after')
                                            Завершено
                                        @endif
                                </span>
                                </div>
                                <div class="lk-body__row-icons">
                                    <div class="v-align">
                                        <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                                        <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                                        <img src="/images/icons/eye.svg"/> {{ $event->views }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="pagination-panel">
                                {{ $events->links('frontend.includes.pagination', ['paginator' => $events]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
