@extends('cabinet.layouts.app')

@push('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/ru.js"></script>

    <script>
        moment.locale('ru');

        var ctx = document.getElementById('myChart');

        var views = {!! $event->views_weekly->mapWithKeys(function ($item) {
                return [$item['date'] => $item['count']];
                }) !!};

        let result = [];

        let now = moment.utc().subtract(6, 'd');
        while (now < moment.utc()) {
            if (views.hasOwnProperty(now.format('YYYY-MM-DD'))) {
                result.push({
                    date: now.clone(),
                    count: views[now.format('YYYY-MM-DD')]
                })
            } else {
                result.push({
                    date: now.clone(),
                    count: 0
                })
            }
            now.add(1, 'd')
        }

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: result.reduce((previous, current) => {
                    previous.push([
                        current.date.format('DD MMM'), current.date.format('dd')
                    ]);
                    return previous;
                }, []),
                datasets: [{
                    label: '# of Votes',
                    data: result.reduce((previous, current) => {
                        previous.push(current.count);
                        return previous;
                    }, []),
                    backgroundColor: [
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)',
                        'rgba(82, 175, 255, 1)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        position: 'right',
                        ticks: {
                            fontSize: 12,
                            fontColor: '#8BA3B9',
                            padding: 20,
                            fontFamily: 'Monsterrat-sbold',
                            beginAtZero: true,
                            callback: function (value) { if (Number.isInteger(value)) { return value; } },
                        },
                        gridLines: {
                            color: '#E9F1F7',
                            zeroLineColor: "#E9F1F7",
                            drawBorder: false,
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 12,
                            fontColor: '#8BA3B9',
                            padding: 15,
                            fontFamily: 'Monsterrat-sbold',
                            beginAtZero: true
                        },
                        gridLines: {
                            drawBorder: true,
                            display: false
                        }
                    }],
                }
            }
        });


    </script>
@endpush

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li><a href="{{ route('cabinet.event.index') }}">События</a></li>
            <li>{{ $event->name }}</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Статистика просмотров
    </div>
    <div class="lk-body__stats">
        <span>{{ $event->views_weekly->sum('count') }}</span> просмотров за неделю
    </div>
    <div class="lk-body__graph">
        <div class="row">
            <div class="col-md-8">
                <canvas id="myChart" style="width:100%" height="430"></canvas>
            </div>
            <div class="col-xl-8 col-lg-12">
                <div class="lk-calc-block">
                    <div class="lk-calc-block-text">Просмотры считаются за счет количества появлений вашего объявления в
                        ленте, когда его увидели другие пользователи. Чем чаще оно встречается в ленте, тем больше
                        покупателей видят его, а значит продать свой товар или услугу можно быстрее.
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12">
                <div class="lk-body__nav mds">
                    <a href="{{ route('cabinet.banner.index') }}">
                        <div class="lk-body__nav-button v-align"><img src="/images/icons/round-add-button.svg"/>Подключить
                            услуги
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
