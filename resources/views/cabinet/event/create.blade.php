@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li><a href="{{ route('cabinet.company.index') }}">События</a></li>
            <li>Добавить событие</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Добавить новое событие
    </div>
    <div class="lk-body__row">
        <div class="container-fluid">
            <events-form></events-form>
        </div>
    </div>
@endsection
