<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('cabinet.includes.meta')
</head>
<body>
<div id="app">
    <header id="lk">
        @include('frontend.includes.header-menu')
    </header>
    <div class="d-flex" id="wrapper">
        <div id="sidebar-wrapper">
            <ul>
                <li><a href="{{ route('cabinet.home') }}"><img src="/images/icons/lk/home.svg" /> Главная</a></li>
                <li><a href="{{ route('cabinet.notification.index') }}"><img src="/images/icons/lk/notify.svg" /> Уведомления</a></li>
            </ul>
            <ul>
                <li class="title">Пользователь</li>
                <li><a href="{{ route('cabinet.profile.index') }}"><img src="/images/icons/lk/user.svg" /> Мой профиль</a></li>
                <li><a href="{{ route('cabinet.favorite.index') }}"><img src="/images/icons/lk/bookmark.svg" /> Избранное</a></li>
                <li><a href="{{ route('cabinet.comment.index') }}"><img src="/images/icons/lk/chat.svg" /> Мои отзывы</a></li>
                <li><a href="{{ route('cabinet.question.index') }}"><img src="/images/icons/lk/chat.svg" /> Мои вопросы</a></li>
            </ul>
            <ul>
                <li class="title">Бизнес аккаунт</li>
                <li><a href="{{ route('cabinet.event.index') }}"><img src="/images/icons/lk/event.svg" /> События</a></li>
                <li><a href="{{ route('cabinet.company.index') }}"><img src="/images/icons/lk/company.svg" /> Компании</a></li>
                <li><a href="{{ route('business.comment.index') }}"><img src="/images/icons/lk/chat.svg" /> Отзывы</a></li>
                <li><a href="{{ route('business.question.index') }}"><img src="/images/icons/lk/chat.svg" /> Вопросы</a></li>
                <li><a href="{{ route('cabinet.invoice.index') }}"><img src="/images/icons/lk/ruble.svg" /> Финансы</a></li>
                <li><a href="{{ route('cabinet.banner.index') }}"><img src="/images/icons/lk/hank.svg" /> Реклама</a></li>
            </ul>
        </div>
        <div id="page-content-wrapper" style="display: flex; flex-direction: column; justify-content: space-between;">
            <div class="container-fluid">
                <div class="lk-body">
                    @yield('content')
                </div>
            </div>
            <div class="to_up">
                <img src="/images/icons/up.svg" />
            </div>
            <footer>
                <div class="container-fluid">
                    <div class="row">
                        @include('frontend.includes.footer-menu')
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <city_select></city_select>
</div>

<!-- Scripts -->
@stack('before-scripts')
<script src="{{ mix('js/cabinet.js') }}"></script>
@stack('after-scripts')

<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        'user' => Auth::user()
      ]) !!};
</script>

</body>
</html>
