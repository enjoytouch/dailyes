@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Мой профиль</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Мой профиль
    </div>
    <div class="lk-body__profile">
        <div class="v-align">
            <div class="cabinet-avatar"><img src="{{ auth()->user()->avatar_url }}"/></div>
            <div class="lk-body__profile-name">
                {{ auth()->user()->name }}
                <span>{{ auth()->user()->email }}</span>
            </div>
        </div>
    </div>
    <div class="lk-body__nav">
        <ul>
            <li class="v-align"><a href="{{ route('cabinet.profile.profile') }}"><img src="/images/icons/lk/edit.svg"/> Редактировать профиль</a></li>
            <li class="v-align"><a href="{{ route('cabinet.profile.password') }}"><img src="/images/icons/lk/password.svg"/> Сменить пароль</a></li>
        </ul>
    </div>
@endsection
