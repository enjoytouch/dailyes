@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Отзывы</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Отзывы
    </div>
    <div class="lk-body__table" style="overflow: auto">
        <table class="text">
            <tbody>
            <tr>
                <td>Текст</td>
                <td>Дата</td>
            </tr>
            @foreach($companies as $company)
                <tr class="non">
                    <td class="cols" colspan="3">Компания {{ $company->name }}</td>
                </tr>
                @foreach($company->comments as $comment)
                    <tr>
                        <td><a href="{{ route('cabinet.comment.show', $comment) }}">{{ $comment->text }}</a></td>
                        <td>{{ $comment->created_at }}</td>
                    </tr>
                @endforeach
            @endforeach
            @foreach($events as $event)
                <tr class="non">
                    <td class="cols" colspan="3">Событие {{ $event->name }}</td>
                </tr>
                @foreach($event->comments as $comment)
                    <tr>
                        <td><a href="{{ route('cabinet.comment.show', $comment) }}">{{ $comment->text }}</a></td>
                        <td>{{ $comment->created_at }}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
