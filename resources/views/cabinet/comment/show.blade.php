@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li><a href="{{ route('cabinet.comment.index') }}">Мои отзывы</a></li>
            <li>Ответ на отзыв</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Ответ на отзыв
    </div>
    <div class="lk-body__row">
        <div class="container-fluid">
            <cabinet-comment-show></cabinet-comment-show>
        </div>
    </div>
@endsection
