@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Компании</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Мои компании
    </div>
    @if($companies->count() == 0)
        <div class="lk-body__text">
            У вас не добавлено компаний.<br>
            Что бы люди узнали о вашей компании вы можете добавить ее на наш портал.
        </div>
        <div class="lk-body__nav">
            <a href="{{ route('cabinet.company.create') }}"><div class="lk-body__nav-button v-align"><img src="/images/icons/round-add-button.svg" />Добавить компанию</div></a>
        </div>
    @else
        <br>
        <div>
            <a href="{{ route('cabinet.company.create') }}"><div class="lk-body__nav-button v-align"><img src="/images/icons/round-add-button.svg" />Добавить компанию</div></a>
        </div>
        <div class="lk-body__row">
            <div class="container-fluid">
                <div class="trigger_company" style="display: block;">
                    <div class="row">
                        @foreach($companies as $company)
                            <div class="col-xl-4 col-lg-6 col-sm-12">
                                <div class="search-panel__company-block">
                                    <div class="search-panel__company-block-img"
                                         style="background-image: url('{{ $company->image_url }}');"></div>
                                    <a href="{{ route('cabinet.company.edit', $company) }}">
                                        <div class="events-block__empty"></div>
                                    </a>
                                    <div class="search-panel__company-group">{{ $company->categories[0]->name }}</div>
                                    <bookmark :event="{{ json_encode($company->only('slug')) }}"
                                              :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_companies->contains('id', $company->id) : false) }}"></bookmark>
                                    <div class="search-panel__company-text">
                                        <div class="search-panel__company-title">{{ $company->name }}</div>
                                        <div class="search-panel__company-place">{{ $company->addresses->count() > 0 ? $company->addresses[0]->address : '' }} {{ $company->addresses->count() > 1 ? ' и еще ' . ($company->addresses->count() - 1) : '' }}
                                        </div>
                                        <div class="search-panel__company-badges">
                                            <img src="/images/icons/thumb-up-black.svg">
                                            <span>{{ $company->rec }}</span>
                                            <img class="star" src="/images/icons/star.svg">
                                            <span>{{ round($company->star, 1) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="pagination-panel">
                                {{ $companies->links('frontend.includes.pagination', ['paginator' => $companies]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
