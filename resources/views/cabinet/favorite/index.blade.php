@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Избранное</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Избранное
    </div>
    <div class="lk-body__trigger">
        <div data="1" class="lk-body__trigger-url active">События<span>{{ $events->count() }}</span></div>
        <div data="2" class="lk-body__trigger-url">Компании<span>{{ $companies->count() }}</span></div>
    </div>
    <div class="lk-body__row">
        <div class="container-fluid">
            <div class="trigger_events">
                <div class="row">

                    @foreach($events as $event)
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div class="lk-body__row-img">
                                <a href="{{ route('frontend.event.show', $event) }}">
                                    <div class="events-block__empty"></div>
                                </a>
                                <img src="{{ $event->image_url }}"/>
                                @if($event->favorite)
                                    <div class="events-block__cool">
                                        <img src="/images/icons/success.svg"/> Выбор редакции
                                    </div>
                                @endif
                                <div class="events-block__badges">
                                    @foreach($event->labels as $label)
                                        <div class="events-block__badges-group">
                                            <div class="miw"><img src="{{ $label->icon_url }}"/></div>
                                            {{ $label->name }}
                                        </div>
                                    @endforeach
                                </div>
                                <bookmark :event="{{ json_encode($event->only('slug')) }}"
                                          :status="{{ json_encode(Auth()->user() ? Auth()->user()->favorite_events->contains('id', $event->id) : false) }}"></bookmark>
                            </div>
                            <div class="lk-body__row-title">
                                <a href="{{ route('frontend.event.show', $event) }}">{{ $event->name }}</a>
                            </div>
                            <span class="lk-body__row-badge">
                                        {{ $event->categories[0]->name }}
                                    </span>
                            <div class="lk-body__row-text">
                                <span>{{ $event->company->name }}</span>
                                <span>
                                    @if($event->status == 'active')
                                        Дата окончания: {{ $event->end->locale('ru')->isoFormat('D MMMM YYYY') }}
                                    @elseif($event->status == 'before')
                                        Дата начала: {{ $event->start->locale('ru')->isoFormat('D MMMM YYYY') }}
                                    @elseif($event->status == 'after')
                                        Завершено
                                    @endif
                                </span>
                            </div>
                            <div class="lk-body__row-icons">
                                <div class="v-align">
                                    <img src="/images/icons/thumb-up-blue.svg"/> {{ $event->rec }}%
                                    <img src="/images/icons/star-blue.svg"/> {{ round($event->star, 1) }}
                                    <img src="/images/icons/eye.svg"/> {{ $event->views }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="trigger_company">
                <div class="row">
                    @foreach($companies as $company)
                        <div class="col-xl-4 col-lg-6 col-sm-12">
                            <div class="search-panel__company-block">
                                <div class="search-panel__company-block-img"
                                     style="background-image: url('{{ $company->image_url }}');"></div>
                                <a href="{{ route('frontend.company.show', $company) }}">
                                    <div class="events-block__empty"></div>
                                </a>
                                <div class="search-panel__company-group">{{ $company->categories[0]->name }}</div>
                                <div class="events-block__badges-bookmark active"></div>
                                <div class="search-panel__company-text">
                                    <div class="search-panel__company-title">{{ $company->name }}</div>
                                    <div class="search-panel__company-place">{{ $company->addresses->count() > 0 ? $company->addresses[0]->address : '' }} {{ $company->addresses->count() > 1 ? ' и еще ' . ($company->addresses->count() - 1) : '' }}
                                    </div>
                                    <div class="search-panel__company-badges">
                                        <img src="/images/icons/thumb-up-black.svg">
                                        <span>{{ $company->rec }}</span>
                                        <img class="star" src="/images/icons/star.svg">
                                        <span>{{ round($company->star, 1) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
