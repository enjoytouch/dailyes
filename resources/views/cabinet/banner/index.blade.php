@extends('cabinet.layouts.app')

@section('content')
    <div class="lk-body__breadcrumbs">
        <ul>
            <li><a href="{{ route('cabinet.home') }}">Личный кабинет</a></li>
            <li>Реклама</li>
        </ul>
    </div>
    <div class="lk-body__title">
        Подключить услуги
    </div>
    <div class="lk-body__desc">
        Подключите услуги, что бы увеличить количество просмотров вашего события.
    </div>
    <div class="lk-body__services">
        <div class="v-align lk-body__services-title">
            <img src="/images/icons/lk-services.svg" />Выделить событие
        </div>
        <div class="lk-body__services-economy">
            Экономия 50%
        </div>
        <div class="lk-body__services-desc">
            Ваше событие будет выделено в ленте в течение 7 дней. Оно будет заметнее<br> стандартных событий и наберет до 20 раз больше просмотров.
        </div>
        <div class="lk-body__services-nav">
            <a href=""><button>Подключить</button></a>
            <div class="lk-body__services-nav-price">
                <strike>1100 ₽</strike>
                <div class="v-align">500 <img src="/images/icons/ruble-currency-sign.svg" /></div>
            </div>
        </div>
    </div>
    <div class="lk-body__services">
        <div class="v-align lk-body__services-title">
            <img src="/images/icons/lk-services.svg" />Поднять событие
        </div>
        <div class="lk-body__services-economy">
            Экономия 50%
        </div>
        <div class="lk-body__services-desc">
            Ваше событие будет поднято в самое начало ленты, тем самым вы привлечете<br> больше внимания аудитории портала.
        </div>
        <div class="lk-body__services-nav">
            <a href=""><button>Подключить</button></a>
            <div class="lk-body__services-nav-price">
                <strike>500 ₽</strike>
                <div class="v-align">300 <img src="/images/icons/ruble-currency-sign.svg" /></div>
            </div>
        </div>
    </div>
    <div class="lk-body__services">
        <div class="v-align lk-body__services-title">
            <img src="/images/icons/lk-services.svg" />Указать на событие
        </div>
        <div class="lk-body__services-economy">
            Экономия 50%
        </div>
        <div class="lk-body__services-desc">
            Ваше событие будет отмечено специальной иконкой «Выбор редакции». Это<br> позволит выделиться на фоне стандартных событий, тем самым позволит набрать<br> до 10 раз больше просмотров
        </div>
        <div class="lk-body__services-nav">
            <a href=""><button>Подключить</button></a>
            <div class="lk-body__services-nav-price">
                <strike>1000 ₽</strike>
                <div class="v-align">800 <img src="/images/icons/ruble-currency-sign.svg" /></div>
            </div>
        </div>
    </div>
@endsection
