import './bootstrap';

import 'owl.carousel/dist/owl.carousel.min';

import 'moment/moment';
import './plugins/daterangepicker';
import './plugins/bootstrap-slider.min';

import 'jquery-validation/dist/jquery.validate.min';

import 'infinite-scroll/dist/infinite-scroll.pkgd.min'

import './plugins/simplebar.min';

import '@fancyapps/fancybox';

window.Vue = require('vue');

// Dependencies --------------------------------------

import Toasted from 'vue-toasted';
import VueClip from 'vue-clip'
import Multiselect from 'vue-multiselect'
import swal from 'sweetalert';
import VueContentPlaceholders from 'vue-content-placeholders'

Vue.use(require('vue-moment'));
Vue.use(Toasted)
Vue.toasted.register('error', message => message, {
    position: 'bottom-center',
    duration: 3500
})
Vue.use(VueClip)
Vue.component('multiselect', Multiselect)
Vue.use(VueContentPlaceholders)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Layout
Vue.component('login', require('./components/auth/Login.vue'));
Vue.component('register', require('./components/auth/Register.vue'));
Vue.component('forgot', require('./components/auth/Forgot.vue'));
Vue.component('reset', require('./components/auth/Reset.vue'));

//City
Vue.component('city_option', require('./components/city/CityOption.vue'));
Vue.component('city_select', require('./components/city/CitySelect.vue'));

// Subscriber Form
Vue.component('subscriber', require('./components/Subscriber.vue'));

// Карта с отображением адресов
Vue.component('addressesMap', require('./components/AddressesMap.vue'));
Vue.component('addressesCarousel', require('./components/AddressesCarousel.vue'));

// Метка избранного
Vue.component('bookmark', require('./components/Bookmark.vue'));
Vue.component('companyBookmark', require('./components/CompanyBookmark.vue'));

// система отзывов рейтинга и оценок
Vue.component('ratingHeader', require('./components/event/RatingHeader.vue'));
Vue.component('recommendationHeader', require('./components/event/RecommendationHeader.vue'));

// система отзывов рейтинга и оценок для компании
Vue.component('companyRatingHeader', require('./components/company/RatingHeader.vue'));
Vue.component('companyRecommendationHeader', require('./components/company/RecommendationHeader.vue'));

Object.defineProperty(Vue.prototype, "$bus", {
    get: function () {
        return this.$root.bus;
    }
});

const router = new VueRouter({
    mode: 'history'
})

const app = new Vue({
    el: '#app',
    router,
    data: {
        bus: new Vue({})
    }
});
