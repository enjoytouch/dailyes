<?php

Route::group([
    'namespace' => 'Subscriber',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'subscriber',
        'as' => 'subscriber.',
    ], function () {
        Route::post('filter', 'SubscriberController@filter')->name('filter');
    });

    /*
     * Resource
     */
    Route::resource('subscriber', 'SubscriberController')->only(['store', 'show', 'update', 'destroy']);
});
