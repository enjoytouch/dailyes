<?php

Route::group([
    'namespace' => 'Category',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'category',
        'as' => 'category.',
    ], function () {
        Route::post('filter', 'CategoryController@filter')->name('filter');
        Route::get('all', 'CategoryController@all')->name('all');
    });

    /*
     * Resource
     */
    Route::resource('category', 'CategoryController')->only(['store', 'show', 'update', 'destroy']);
});
