<?php

Route::group([
    'namespace' => 'Report',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'report',
        'as' => 'report.',
    ], function () {
        Route::post('filter', 'ReportController@filter')->name('filter');
    });
});
