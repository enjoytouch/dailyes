<?php

Route::group([
    'namespace' => 'Comment',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'comment',
        'as' => 'comment.',
    ], function () {
        Route::post('filter', 'CommentController@filter')->name('filter');
        require __DIR__ . '/comment/answer.php';
        Route::group([
            'prefix' => '{comment}',
        ], function () {
            Route::post('toggle', 'CommentController@toggle')->name('toggle');
        });
    });

    /*
     * Resource
     */
    Route::resource('comment', 'CommentController')->only(['destroy']);
});
