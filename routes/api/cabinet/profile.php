<?php

Route::group([
    'namespace' => 'Profile',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'profile',
        'as' => 'profile.',
    ], function () {
        Route::get('/getAuthUser', 'ProfileController@getAuthUser')->middleware('permission:read-profile');
        Route::put('/updateAuthUser', 'ProfileController@updateAuthUser')->middleware('permission:update-profile');
        Route::put('/updatePasswordAuthUser', 'ProfileController@updatePasswordAuthUser')->middleware('permission:update-profile-password');
        Route::post('/uploadAvatarAuthUser', 'ProfileController@uploadAvatarAuthUser')->middleware('permission:update-profile');
        Route::post('/removeAvatarAuthUser', 'ProfileController@removeAvatarAuthUser')->middleware('permission:update-profile');
    });

    /*
     * Resource
     */
});
