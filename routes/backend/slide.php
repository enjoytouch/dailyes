<?php

Route::group([
    'namespace' => 'Slide',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'slide',
        'as' => 'slide.',
    ], function () {

    });

    /*
     * Resource
     */
    Route::resource('slide', 'SlideController')->only(['index', 'create', 'edit']);
});
