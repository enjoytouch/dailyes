<?php

Route::group([
    'namespace' => 'Report',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'report',
        'as' => 'report.',
    ], function () {

    });

    /*
     * Resource
     */
    Route::resource('report', 'ReportController')->only(['index']);
});
