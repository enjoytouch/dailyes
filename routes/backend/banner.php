<?php

Route::group([
    'namespace' => 'Banner',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'banner',
        'as' => 'banner.',
    ], function () {
        require __DIR__ . '/banner/place.php';
    });

    /*
     * Resource
     */
    Route::resource('banner', 'BannerController')->only(['index', 'create', 'edit']);
});
