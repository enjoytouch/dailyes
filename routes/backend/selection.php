<?php

Route::group([
    'namespace' => 'Selection',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'selection',
        'as' => 'selection.',
    ], function () {

    });

    /*
     * Resource
     */
    Route::resource('selection', 'SelectionController')->only(['index', 'create', 'edit']);
});
