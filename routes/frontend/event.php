<?php

Route::group([
    'namespace' => 'Event',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'events',
        'as' => 'event.',
    ], function () {
        Route::get('/', 'EventController@index')->name('index');
        Route::get('{event}', 'EventController@show')->name('show');
        Route::group([
            'prefix' => '{event}',
            'middleware' => 'auth'
        ], function () {
            Route::post('/review', 'EventController@review')->name('review');
            Route::post('/question', 'EventController@question')->name('question');
            Route::post('/report', 'EventController@report')->name('report');
        });
    });

    /*
     * Resource
     */
});
