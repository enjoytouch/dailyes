<?php

Route::group([
    'namespace' => 'Event',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'events',
        'as' => 'event.',
    ], function () {
        require __DIR__ . '/event/category.php';
    });

    /*
     * Resource
     */
});
