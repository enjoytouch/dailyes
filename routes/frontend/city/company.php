<?php

Route::group([
    'namespace' => 'Company',
], function () {
    /*
     * Additional
     */
    Route::group([
        'prefix' => 'companies',
        'as' => 'company.',
    ], function () {
        require __DIR__ . '/company/category.php';
    });

    /*
     * Resource
     */
});
