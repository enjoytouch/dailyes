<?php

Route::group([
    'namespace' => 'Category',
], function () {
    /*
     * Additional
     */
    Route::group([
        'as' => 'category.',
    ], function () {
        Route::get('{category?}', 'CategoryController@show')->name('show');
    });

    /*
     * Resource
     */
});
