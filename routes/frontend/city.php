<?php

Route::group([
    'namespace' => 'City',
], function () {
    /*
     * Additional
     */
    Route::group([
        'as' => 'city.',
    ], function () {
        Route::get('{city?}', 'CityController@show')->name('show');
        Route::group([
            'prefix' => '{city}',
        ], function () {
            require __DIR__ . '/city/search.php';
            require __DIR__ . '/city/event.php';
            require __DIR__ . '/city/company.php';
        });
    });

    /*
     * Resource
     */
});
